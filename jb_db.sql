--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: autobet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.autobet (
    id integer NOT NULL,
    uid numeric,
    created timestamp with time zone,
    stake numeric,
    cashout numeric(10,2),
    bets integer,
    on_win character varying,
    on_lose character varying,
    stop_profit numeric(10,2),
    stop_lose numeric(10,2),
    sys_status character varying,
    sys_profit numeric(10,2)
);


ALTER TABLE public.autobet OWNER TO postgres;

--
-- Name: autobet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.autobet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.autobet_id_seq OWNER TO postgres;

--
-- Name: autobet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.autobet_id_seq OWNED BY public.autobet.id;


--
-- Name: bankroll; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.bankroll (
    game text,
    amount numeric DEFAULT 0,
    weight integer DEFAULT 102,
    edge_percentage integer DEFAULT 4,
    edge_tolerance integer DEFAULT 200000,
    formula_to_use character varying DEFAULT 'bc'::character varying,
    id integer NOT NULL,
    win_per_round numeric(6,3) DEFAULT 1.125,
    win_per_user numeric(6,3) DEFAULT 0.75,
    pause_bets boolean DEFAULT false,
    app_version character varying DEFAULT '20220101001'::character varying,
    chat_amount integer DEFAULT 5000
);


ALTER TABLE public.bankroll OWNER TO bust;

--
-- Name: bankroll_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.bankroll_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bankroll_id_seq OWNER TO bust;

--
-- Name: bankroll_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.bankroll_id_seq OWNED BY public.bankroll.id;


--
-- Name: bets; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.bets (
    game text,
    gid text,
    uid bigint,
    result json,
    created timestamp without time zone DEFAULT now(),
    profit numeric,
    hash text,
    name text,
    cashout numeric,
    coin text,
    amount numeric,
    excise real DEFAULT 0,
    wht numeric DEFAULT 0,
    profit_tax numeric DEFAULT 0,
    id bigint NOT NULL,
    type character varying DEFAULT 'c'::character varying,
    option character varying DEFAULT 'm'::character varying,
    user_type character(1) DEFAULT 'u'::bpchar,
    balance numeric(15,2)
);


ALTER TABLE public.bets OWNER TO bust;

--
-- Name: bets_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.bets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.bets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: bonus_earnings; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.bonus_earnings (
    id integer NOT NULL,
    uid numeric,
    bet_amount numeric(10,2),
    bonus numeric(10,2),
    created timestamp without time zone DEFAULT now(),
    bonus_rate integer DEFAULT 10,
    source character varying DEFAULT 'happy hour'::character varying
);


ALTER TABLE public.bonus_earnings OWNER TO bust;

--
-- Name: bonus_earnings_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.bonus_earnings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bonus_earnings_id_seq OWNER TO bust;

--
-- Name: bonus_earnings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.bonus_earnings_id_seq OWNED BY public.bonus_earnings.id;


--
-- Name: bots; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.bots (
    id numeric,
    status text,
    "time" numeric,
    coin text,
    game text,
    bot_id integer NOT NULL,
    amount integer DEFAULT 500,
    cashout numeric(5,2) DEFAULT 2.2,
    f_uid numeric DEFAULT 0,
    start_time time without time zone DEFAULT '00:00:00'::time without time zone,
    end_time time without time zone DEFAULT '08:00:00'::time without time zone,
    type character varying DEFAULT 'normal'::character varying,
    button character varying DEFAULT 'm'::character varying
);


ALTER TABLE public.bots OWNER TO bust;

--
-- Name: bots_bot_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.bots_bot_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bots_bot_id_seq OWNER TO bust;

--
-- Name: bots_bot_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.bots_bot_id_seq OWNED BY public.bots.bot_id;


--
-- Name: chat_global; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.chat_global (
    name text,
    message text,
    date timestamp with time zone DEFAULT now(),
    uid numeric,
    "time" text,
    created timestamp with time zone DEFAULT now(),
    room text,
    sorter numeric,
    avatar text,
    level numeric DEFAULT 1,
    id bigint NOT NULL,
    show boolean DEFAULT true
);


ALTER TABLE public.chat_global OWNER TO bust;

--
-- Name: chat_global_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.chat_global ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.chat_global_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: chat_spam; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.chat_spam (
    uid numeric,
    date timestamp with time zone DEFAULT now(),
    room text,
    "time" text,
    created timestamp with time zone DEFAULT now(),
    name text,
    message text,
    sorter numeric,
    avatar text,
    level numeric DEFAULT 1,
    id bigint NOT NULL
);


ALTER TABLE public.chat_spam OWNER TO bust;

--
-- Name: chat_spam_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.chat_spam ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.chat_spam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: crashs; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.crashs (
    gid text,
    busted numeric,
    hash text,
    date timestamp with time zone DEFAULT now(),
    weight numeric DEFAULT 100,
    id bigint NOT NULL,
    bankroll numeric(15,2) DEFAULT 0.00,
    edge numeric DEFAULT 0,
    crash_pedro numeric,
    crash_bust numeric,
    crash_paka numeric,
    crash_use character varying,
    crash_bc numeric
);


ALTER TABLE public.crashs OWNER TO bust;

--
-- Name: crashs_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.crashs ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.crashs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: deposits; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.deposits (
    uid numeric,
    date timestamp without time zone DEFAULT now(),
    status text,
    txtid text,
    salt text,
    coin text,
    amount numeric,
    id bigint NOT NULL,
    excise numeric DEFAULT 0
);


ALTER TABLE public.deposits OWNER TO bust;

--
-- Name: deposits_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.deposits ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.deposits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: jackpot; Type: TABLE; Schema: public; Owner: vas
--

CREATE TABLE public.jackpot (
    id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    play_amount integer DEFAULT 100,
    slots integer DEFAULT 0,
    bought_slots integer DEFAULT 0,
    completion integer DEFAULT 0,
    winner_amounts jsonb
);


ALTER TABLE public.jackpot OWNER TO vas;

--
-- Name: jackpot_bets; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.jackpot_bets (
    id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    uid bigint DEFAULT 0,
    jackpot_id integer DEFAULT 0,
    amount numeric(10,2) DEFAULT 0,
    excise numeric(10,2) DEFAULT 0,
    cashout numeric(8,2) DEFAULT 0,
    result numeric(8,2) DEFAULT 0,
    gid bigint DEFAULT 0
);


ALTER TABLE public.jackpot_bets OWNER TO bust;

--
-- Name: jackpot_bets_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.jackpot_bets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.jackpot_bets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: jackpot_id_seq; Type: SEQUENCE; Schema: public; Owner: vas
--

ALTER TABLE public.jackpot ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.jackpot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: jackpot_quora; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jackpot_quora (
    id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    jackpot_id integer,
    level integer,
    slots_registered integer,
    slots_qualified integer,
    status integer DEFAULT 0
);


ALTER TABLE public.jackpot_quora OWNER TO postgres;

--
-- Name: jackpot_quora_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jackpot_quora_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jackpot_quora_id_seq OWNER TO postgres;

--
-- Name: jackpot_quora_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jackpot_quora_id_seq OWNED BY public.jackpot_quora.id;


--
-- Name: jackpot_slot; Type: TABLE; Schema: public; Owner: vas
--

CREATE TABLE public.jackpot_slot (
    id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    jackpot_id integer DEFAULT 0,
    uid bigint,
    level integer DEFAULT 1,
    amount numeric(10,2) DEFAULT 0,
    excise numeric(10,2) DEFAULT 0,
    retries integer DEFAULT 0,
    status integer DEFAULT 0,
    cashout numeric(6,2) DEFAULT 0,
    rank integer DEFAULT 0,
    gid integer DEFAULT 0,
    quora_id integer
);


ALTER TABLE public.jackpot_slot OWNER TO vas;

--
-- Name: jackpot_slot_id_seq; Type: SEQUENCE; Schema: public; Owner: vas
--

ALTER TABLE public.jackpot_slot ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.jackpot_slot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: logs; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.logs (
    id integer,
    info text,
    log_id integer NOT NULL
);


ALTER TABLE public.logs OWNER TO bust;

--
-- Name: logs_log_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.logs_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logs_log_id_seq OWNER TO bust;

--
-- Name: logs_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.logs_log_id_seq OWNED BY public.logs.log_id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.messages (
    id bigint NOT NULL,
    from_uid numeric,
    to_uid numeric,
    message text,
    "time" character varying,
    from_name character varying,
    to_name character varying,
    room_key character varying,
    date_created timestamp without time zone DEFAULT now()
);


ALTER TABLE public.messages OWNER TO bust;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO bust;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.notifications (
    title text,
    content text,
    date timestamp with time zone DEFAULT now(),
    id bigint NOT NULL
);


ALTER TABLE public.notifications OWNER TO bust;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.notifications ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ref_earnings; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.ref_earnings (
    id integer NOT NULL,
    from_uid numeric NOT NULL,
    to_uid numeric NOT NULL,
    amount numeric(10,2) DEFAULT 0,
    earnings numeric(10,2) DEFAULT 0,
    created timestamp without time zone DEFAULT now(),
    referral_rate integer DEFAULT 2
);


ALTER TABLE public.ref_earnings OWNER TO bust;

--
-- Name: ref_earnings_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.ref_earnings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ref_earnings_id_seq OWNER TO bust;

--
-- Name: ref_earnings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.ref_earnings_id_seq OWNED BY public.ref_earnings.id;


--
-- Name: refferals; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.refferals (
    uid numeric NOT NULL,
    created timestamp with time zone DEFAULT now(),
    earned numeric NOT NULL,
    username text NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.refferals OWNER TO bust;

--
-- Name: refferals_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.refferals ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.refferals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    setting_key character varying DEFAULT 0,
    setting_value character varying DEFAULT 0,
    status integer DEFAULT 0
);


ALTER TABLE public.settings OWNER TO bust;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO bust;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: sms_bets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sms_bets (
    id integer NOT NULL,
    uid bigint,
    status character varying,
    amount numeric,
    cashout numeric,
    won boolean,
    result numeric,
    game_id numeric,
    created timestamp with time zone
);


ALTER TABLE public.sms_bets OWNER TO postgres;

--
-- Name: sms_bets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sms_bets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sms_bets_id_seq OWNER TO postgres;

--
-- Name: sms_bets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sms_bets_id_seq OWNED BY public.sms_bets.id;


--
-- Name: sys_hashes; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.sys_hashes (
    game_id bigint,
    hash text
);


ALTER TABLE public.sys_hashes OWNER TO bust;

--
-- Name: tokens; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.tokens (
    uid numeric,
    key text,
    id bigint NOT NULL
);


ALTER TABLE public.tokens OWNER TO bust;

--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.tokens ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.transaction (
    id bigint NOT NULL,
    type_id integer,
    credit_type character varying,
    uid bigint,
    created timestamp without time zone DEFAULT now(),
    amount numeric(15,2),
    old_balance numeric(15,2),
    new_balance numeric(15,2),
    reference character varying DEFAULT '0'::character varying
);


ALTER TABLE public.transaction OWNER TO bust;

--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_id_seq OWNER TO bust;

--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;


--
-- Name: transaction_type; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.transaction_type (
    type_id integer NOT NULL,
    title character varying,
    description character varying
);


ALTER TABLE public.transaction_type OWNER TO bust;

--
-- Name: transaction_type_type_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.transaction_type_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_type_type_id_seq OWNER TO bust;

--
-- Name: transaction_type_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.transaction_type_type_id_seq OWNED BY public.transaction_type.type_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.users (
    id numeric,
    level numeric DEFAULT 1,
    avatar text,
    name text,
    token text,
    password text,
    password2 text,
    email text,
    wallet json,
    muted boolean DEFAULT false,
    created timestamp with time zone DEFAULT now(),
    profit_high json,
    profit_low json,
    max_profit json,
    profit json,
    balance json,
    friends text,
    phone numeric NOT NULL,
    bonus numeric DEFAULT 0,
    plays numeric DEFAULT 0,
    full_name character varying,
    type character varying DEFAULT 'person'::character varying,
    user_id bigint NOT NULL,
    referrer_id integer DEFAULT 0,
    ref_earnings numeric DEFAULT '0'::real,
    status character varying DEFAULT 'Active'::character varying,
    plays_today integer DEFAULT 0,
    bonus_end_time timestamp without time zone,
    chat_status character varying DEFAULT 'Restricted'::character varying,
    minimum_play integer DEFAULT 0
);


ALTER TABLE public.users OWNER TO bust;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.users ALTER COLUMN user_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1
);


--
-- Name: wallets; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.wallets (
    address text,
    uid numeric,
    coin text,
    network text,
    id bigint NOT NULL
);


ALTER TABLE public.wallets OWNER TO bust;

--
-- Name: wallets_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.wallets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.wallets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: winners; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.winners (
    id integer NOT NULL,
    uid numeric NOT NULL,
    phone bigint NOT NULL,
    names character varying,
    payment_code character varying,
    created timestamp without time zone DEFAULT now(),
    status character varying,
    amount_won integer DEFAULT 0,
    narration character varying,
    b2c_request_id integer DEFAULT 0,
    promotion character varying DEFAULT 'default'::character varying
);


ALTER TABLE public.winners OWNER TO bust;

--
-- Name: winners_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

CREATE SEQUENCE public.winners_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.winners_id_seq OWNER TO bust;

--
-- Name: winners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bust
--

ALTER SEQUENCE public.winners_id_seq OWNED BY public.winners.id;


--
-- Name: withdrawals; Type: TABLE; Schema: public; Owner: bust
--

CREATE TABLE public.withdrawals (
    uid numeric,
    date timestamp without time zone DEFAULT now(),
    amount numeric,
    wallet text,
    status text,
    coin text,
    id bigint NOT NULL,
    charges real DEFAULT 0,
    amount_sent real DEFAULT 0,
    wht numeric DEFAULT 0,
    request_id integer DEFAULT 0,
    resend integer DEFAULT 1,
    next_send timestamp without time zone DEFAULT now(),
    sys_safe bigint
);


ALTER TABLE public.withdrawals OWNER TO bust;

--
-- Name: withdrawals_id_seq; Type: SEQUENCE; Schema: public; Owner: bust
--

ALTER TABLE public.withdrawals ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.withdrawals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: autobet id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.autobet ALTER COLUMN id SET DEFAULT nextval('public.autobet_id_seq'::regclass);


--
-- Name: bankroll id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bankroll ALTER COLUMN id SET DEFAULT nextval('public.bankroll_id_seq'::regclass);


--
-- Name: bonus_earnings id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bonus_earnings ALTER COLUMN id SET DEFAULT nextval('public.bonus_earnings_id_seq'::regclass);


--
-- Name: bots bot_id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bots ALTER COLUMN bot_id SET DEFAULT nextval('public.bots_bot_id_seq'::regclass);


--
-- Name: jackpot_quora id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jackpot_quora ALTER COLUMN id SET DEFAULT nextval('public.jackpot_quora_id_seq'::regclass);


--
-- Name: logs log_id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.logs ALTER COLUMN log_id SET DEFAULT nextval('public.logs_log_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: ref_earnings id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.ref_earnings ALTER COLUMN id SET DEFAULT nextval('public.ref_earnings_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: sms_bets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sms_bets ALTER COLUMN id SET DEFAULT nextval('public.sms_bets_id_seq'::regclass);


--
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);


--
-- Name: transaction_type type_id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.transaction_type ALTER COLUMN type_id SET DEFAULT nextval('public.transaction_type_type_id_seq'::regclass);


--
-- Name: winners id; Type: DEFAULT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.winners ALTER COLUMN id SET DEFAULT nextval('public.winners_id_seq'::regclass);


--
-- Name: bankroll bankroll_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bankroll
    ADD CONSTRAINT bankroll_pk PRIMARY KEY (id);


--
-- Name: bets bets_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bets
    ADD CONSTRAINT bets_pkey PRIMARY KEY (id);


--
-- Name: bonus_earnings bonus_earnings_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bonus_earnings
    ADD CONSTRAINT bonus_earnings_pk PRIMARY KEY (id);


--
-- Name: bots bots_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bots
    ADD CONSTRAINT bots_pk PRIMARY KEY (bot_id);


--
-- Name: chat_global chat_global_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.chat_global
    ADD CONSTRAINT chat_global_pkey PRIMARY KEY (id);


--
-- Name: chat_spam chat_spam_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.chat_spam
    ADD CONSTRAINT chat_spam_pkey PRIMARY KEY (id);


--
-- Name: crashs crashs_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.crashs
    ADD CONSTRAINT crashs_pkey PRIMARY KEY (id);


--
-- Name: deposits deposits_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.deposits
    ADD CONSTRAINT deposits_pkey PRIMARY KEY (id);


--
-- Name: jackpot jackpot_pk; Type: CONSTRAINT; Schema: public; Owner: vas
--

ALTER TABLE ONLY public.jackpot
    ADD CONSTRAINT jackpot_pk PRIMARY KEY (id);


--
-- Name: jackpot_quora jackpot_quora_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jackpot_quora
    ADD CONSTRAINT jackpot_quora_pk PRIMARY KEY (id);


--
-- Name: logs logs_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.logs
    ADD CONSTRAINT logs_pk PRIMARY KEY (log_id);


--
-- Name: messages messages_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pk PRIMARY KEY (id);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: ref_earnings ref_earnings_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.ref_earnings
    ADD CONSTRAINT ref_earnings_pk PRIMARY KEY (id);


--
-- Name: refferals refferals_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.refferals
    ADD CONSTRAINT refferals_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pk PRIMARY KEY (id);


--
-- Name: sms_bets sms_bets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sms_bets
    ADD CONSTRAINT sms_bets_pkey PRIMARY KEY (id);


--
-- Name: tokens tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: transaction transaction_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pk PRIMARY KEY (id);


--
-- Name: transaction_type transaction_type_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.transaction_type
    ADD CONSTRAINT transaction_type_pk PRIMARY KEY (type_id);


--
-- Name: users unq_name; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unq_name UNIQUE (name);


--
-- Name: users unqusrid; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT unqusrid UNIQUE (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: wallets wallets_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.wallets
    ADD CONSTRAINT wallets_pkey PRIMARY KEY (id);


--
-- Name: winners winners_pk; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.winners
    ADD CONSTRAINT winners_pk PRIMARY KEY (id);


--
-- Name: withdrawals withdrawals_pkey; Type: CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.withdrawals
    ADD CONSTRAINT withdrawals_pkey PRIMARY KEY (id);


--
-- Name: bankroll_game_uindex; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX bankroll_game_uindex ON public.bankroll USING btree (game);


--
-- Name: chat_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX chat_idx ON public.users USING btree (chat_status);


--
-- Name: date_created; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX date_created ON public.bets USING btree (created);


--
-- Name: fki_chttousr; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX fki_chttousr ON public.chat_global USING btree (uid);


--
-- Name: fki_deptousr; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX fki_deptousr ON public.deposits USING btree (uid);


--
-- Name: fki_withtousr; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX fki_withtousr ON public.withdrawals USING btree (uid);


--
-- Name: fromuid_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX fromuid_idx ON public.ref_earnings USING btree (from_uid);


--
-- Name: gid; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX gid ON public.bets USING btree (gid);


--
-- Name: gid_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX gid_idx ON public.sys_hashes USING btree (game_id);


--
-- Name: giduid_unq; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX giduid_unq ON public.bets USING btree (gid, uid);


--
-- Name: idx_date; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_date ON public.crashs USING btree (date);


--
-- Name: idx_depo_date; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_depo_date ON public.deposits USING btree (date);


--
-- Name: idx_gid; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_gid ON public.crashs USING btree (gid);


--
-- Name: idx_name; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_name ON public.bets USING btree (name);


--
-- Name: idx_plays; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_plays ON public.users USING btree (plays);


--
-- Name: idx_promo; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_promo ON public.winners USING btree (promotion);


--
-- Name: idx_sys_safe; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX idx_sys_safe ON public.withdrawals USING btree (sys_safe, uid);


--
-- Name: idx_type; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX idx_type ON public.users USING btree (type);


--
-- Name: index_created; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX index_created ON public.transaction USING btree (created);


--
-- Name: index_ref; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX index_ref ON public.transaction USING btree (reference);


--
-- Name: jackgid_k; Type: INDEX; Schema: public; Owner: vas
--

CREATE INDEX jackgid_k ON public.jackpot_slot USING btree (gid);


--
-- Name: jackpot_id_uindex; Type: INDEX; Schema: public; Owner: vas
--

CREATE UNIQUE INDEX jackpot_id_uindex ON public.jackpot USING btree (id);


--
-- Name: jackpot_quora_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX jackpot_quora_id_uindex ON public.jackpot_quora USING btree (id);


--
-- Name: onkey; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX onkey ON public.tokens USING btree (key);


--
-- Name: option_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX option_idx ON public.bets USING btree (option);


--
-- Name: phone; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX phone ON public.users USING btree (phone);


--
-- Name: touid_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX touid_idx ON public.ref_earnings USING btree (to_uid);


--
-- Name: transaction_id_uindex; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX transaction_id_uindex ON public.transaction USING btree (id);


--
-- Name: transaction_type_type_id_uindex; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX transaction_type_type_id_uindex ON public.transaction_type USING btree (type_id);


--
-- Name: trx_unique; Type: INDEX; Schema: public; Owner: bust
--

CREATE UNIQUE INDEX trx_unique ON public.deposits USING btree (txtid);


--
-- Name: type_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX type_idx ON public.bets USING btree (type);


--
-- Name: uid; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX uid ON public.bets USING btree (uid);


--
-- Name: uid_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX uid_idx ON public.bonus_earnings USING btree (uid);


--
-- Name: ut_idx; Type: INDEX; Schema: public; Owner: bust
--

CREATE INDEX ut_idx ON public.bets USING btree (user_type);


--
-- Name: bets betstousr; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.bets
    ADD CONSTRAINT betstousr FOREIGN KEY (uid) REFERENCES public.users(id) NOT VALID;


--
-- Name: jackpot_bets btousr; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.jackpot_bets
    ADD CONSTRAINT btousr FOREIGN KEY (uid) REFERENCES public.users(id);


--
-- Name: chat_global chttousr; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.chat_global
    ADD CONSTRAINT chttousr FOREIGN KEY (uid) REFERENCES public.users(id) NOT VALID;


--
-- Name: deposits deptousr; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.deposits
    ADD CONSTRAINT deptousr FOREIGN KEY (uid) REFERENCES public.users(id) NOT VALID;


--
-- Name: transaction fk_trans_type; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT fk_trans_type FOREIGN KEY (type_id) REFERENCES public.transaction_type(type_id);


--
-- Name: transaction fk_user_uid; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT fk_user_uid FOREIGN KEY (uid) REFERENCES public.users(id);


--
-- Name: jackpot_bets jackpot_bets_jackpot_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.jackpot_bets
    ADD CONSTRAINT jackpot_bets_jackpot_id_fk FOREIGN KEY (jackpot_id) REFERENCES public.jackpot(id);


--
-- Name: jackpot_slot jackpot_slot_jackpot_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: vas
--

ALTER TABLE ONLY public.jackpot_slot
    ADD CONSTRAINT jackpot_slot_jackpot_id_fk FOREIGN KEY (jackpot_id) REFERENCES public.jackpot(id);


--
-- Name: jackpot_quora quoratojak; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jackpot_quora
    ADD CONSTRAINT quoratojak FOREIGN KEY (jackpot_id) REFERENCES public.jackpot(id);


--
-- Name: jackpot_slot slttoquo; Type: FK CONSTRAINT; Schema: public; Owner: vas
--

ALTER TABLE ONLY public.jackpot_slot
    ADD CONSTRAINT slttoquo FOREIGN KEY (quora_id) REFERENCES public.jackpot_quora(id);


--
-- Name: jackpot_slot slttouid; Type: FK CONSTRAINT; Schema: public; Owner: vas
--

ALTER TABLE ONLY public.jackpot_slot
    ADD CONSTRAINT slttouid FOREIGN KEY (uid) REFERENCES public.users(id);


--
-- Name: withdrawals withtousr; Type: FK CONSTRAINT; Schema: public; Owner: bust
--

ALTER TABLE ONLY public.withdrawals
    ADD CONSTRAINT withtousr FOREIGN KEY (uid) REFERENCES public.users(id) NOT VALID;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO bust;
GRANT ALL ON SCHEMA public TO vas;


--
-- Name: TABLE bankroll; Type: ACL; Schema: public; Owner: bust
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.bankroll TO vas;


--
-- PostgreSQL database dump complete
--

