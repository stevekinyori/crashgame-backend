let sys_times = 0;
let sys_interval = 0;

let t_c = clearInterval(sys_interval);
console.log('t_c: ' + t_c);

function checkSysStatus(io) {
    console.log('[' + sys_times + ' | ' + sys_interval + '] sys status: ' + Math.random() + " | " + io);
    sys_times++;

    if (sys_times == 3) {
        clearInterval(sys_interval);
    }
}

sys_interval = setInterval(() => {
    checkSysStatus('123');
}, (3 * 1000));
console.log('interval id: ' + sys_interval);