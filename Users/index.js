const _ = require('lodash');
const Token = require('./Token');
const Rule = require('./Rule');
const C = require('../General/Constant');
const {encode, decode} = require('../General/Buffer');
const axios = require('axios');
const H = require('../General/Helper');
const configs = require("../config");

let clientsIP = [];

/*
 * User Socket Handler
*/
function User(client, io, siofu) {
    /**
     * Authentication User
     */
    client.on(C.ONLINE, (data) => {

        let {token, identifier} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.authentication(client, id, identifier, (result) => {
                client.emit(C.ONLINE, encode(result));
            })
        })
    });

    /**
     * Logout User
     */
    client.on(C.LOGOUT_USER, (data) => {
        let {token} = decode(data);
        Token.getID(token, (id) => {
            Token.refresh(id, () => {
            })
        })
    });

    /**
     * Get User ID
     */
    client.on(C.GET_UID, (data) => {
        let {token} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            client.emit(C.GET_UID, encode(id));
        })
    });

    /**
     * Client Login
     */
    client.on(C.LOGIN_USER, (data) => {

        //console.log(H.getLogTime() + " [temp] LOGIN_USER hit: ", data);

        let {phone, password, recaptcha} = decode(data);
        Rule.login(phone, password, recaptcha, client.id, (result) => {
            client.emit(C.LOGIN_USER, encode(result));
        });
    });

    /**
     * Client Login by Google
     */
    client.on(C.LOGIN_USER_GOOGLE, (data) => {
        let {username, email, token} = decode(data);
        Rule.loginByGoogle(username, email, client.id, (result) => {
            client.emit(C.LOGIN_USER, encode(result));
        });
    });

    /**
     * Client Register
     */
    client.on(C.REGISTER_USER, (data) => {
        let {username, password, email, phone, method, aff} = decode(data);

        // let ip = client.request.connection.remoteAddress;

        // if(_.isUndefined(ip))
        //     ip = client.handshake.headers['x-forwarded-for'];

        // if(!_.isUndefined(aff))
        // {
        //     if(!_.includes(clientsIP, ip))
        //     {
        //         clientsIP.push(ip)
        //         Rule.register(username, password, email, phone, method, aff, (result) => {
        //             client.emit(C.REGISTER_USER, encode(result));
        //         });
        //     }
        //     else {
        //         client.emit(C.REGISTER_USER, encode({ status: 'Trying again will make you ban.' }));
        //     }
        // }
        // else 
        // {
        Rule.register(username, password, email, phone, method, aff, (result) => {
            client.emit(C.REGISTER_USER, encode(result));
        });
        // }
    });

    client.on(C.DEPOSIT, (data) => {
        let {phone, amount} = decode(data);
        let phone_ = phone.replace('254', '0');

        //console.log(H.getLogTime() + " [temp] stk: " + phone + " | " + amount);

        //do stk
        let data_ = JSON.stringify({
            "msisdn": phone,
            "amount": amount,
            "paybill": configs.PAYBILL,
            "stk_acc_ref": configs.PAYBILL_ACCOUNT, //phone_,
            "stk_trans_desc": configs.PAYBILL_ACCOUNT,
            "result_url": configs.STK_CALLBACK,
            "client_ref": Date.now(),
        });

        let config = {
            method: 'post',
            url: 'https://pay.vashub.co.ke/stk',
            headers: {
                'h_api_key': configs.PAYMENT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data_
        };

        axios(config)
            .then(function (response) {
                //console.log(H.getLogTime() + " [temp] stk: " + phone + " | " + amount + " >> " + JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(H.getLogTime() + " [temp] stk: " + phone + " | " + amount + " >> error: " + error);
            });
    });

    client.on(C.CONFIRM, (data) => {
        let {phone, mpesa_code} = decode(data);

        console.log(H.getLogTime() + " | confirm c2b: " + phone + " | " + mpesa_code);

        //do confirm
        let data_ = JSON.stringify({
            "msisdn": phone,
            "mpesa_code": mpesa_code,
            "paybill": configs.PAYBILL,
            "client_ref": Date.now(),
        });

        let config = {
            method: 'post',
            url: 'https://pay.vashub.co.ke/c2btrx',
            headers: {
                'h_api_key': configs.PAYMENT_API_KEY,
                'Content-Type': 'application/json'
            },
            data: data_
        };

        axios(config)
            .then(function (response) {
                console.log(H.getLogTime() + "confirm c2b: " + phone + " | " + mpesa_code + " >> " + JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(H.getLogTime() + "confirm c2b: " + phone + " | " + mpesa_code + " >> error: " + error);
            });
    });

    /**
     * Get the referral list
     */
    client.on(C.MY_AFF, (data) => {
        let {token} = decode(data);
        Token.getUserIdByToken(token, (user_id, ref_earnings) => {
            if (!user_id) return;
            Rule.allReferral(user_id, (result, count) => {
                client.emit(C.MY_AFF, encode(result));
                client.emit(C.MY_REFERRAL, encode({'referral_count': count, 'earnings': ref_earnings}));
            });
        })
    });

    client.on(C.REF_EARNINGS, (data) => {
        let {token, game} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.earningsTrx(id, (result) => {
                client.emit(C.REF_EARNINGS, encode(result));
            });
        })
    });

    client.on('HAPPY_HOUR_TODAY', (data) => {
        let {token, game} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.happyHourToday(id, (earnings_today, total, ending_time) => {
                client.emit('HAPPY_HOUR_TODAY', encode({'earnings_today': earnings_today, 'earnings': total, 'ending_time': ending_time}));
            })
        })
    });

    client.on('HAPPY_HOUR_EARNINGS', (data) => {
        let {token, game} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.topHappyHour(20, (result) => {
                client.emit('HAPPY_HOUR_EARNINGS', encode(result));
            });
        })
    });

    /**
     * Client Reset Password
     */
    client.on(C.RESET_PASSWORD, (data) => {
        let {phone} = decode(data);
        if (!phone) return;
        Rule.resetClientPassword(phone, (result) => {
            client.emit(C.RESET_PASSWORD, encode(result));
        });
    });

    /**
     * Client Credit
     */
    client.on(C.CREDIT, (data) => {
        let {token} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.getClientCredit(id, (result) => {
                client.emit(C.CREDIT, encode({credit: result}));
            })
        })
    });

    /**
     * Client Bets
     */
    client.on(C.MY_BETS, (data) => {
        let {token, game} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.getClientBets(id, game, (result) => {
                client.emit(C.MY_BETS, encode({history: result}));
            })
        })
    });

    /**
     * Client History
     */
    client.on(C.MY_HISTORY, (data) => {
        let {token} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.getClientHistory(id, (result) => {
                client.emit(C.MY_HISTORY, encode({history: result}));
            })
        })
    });

    /**
     * Add a Chat
     */
    client.on(C.ADD_CHAT, (data) => {
        let {token, country, message} = data;
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.addChat(id, country, message, io, (result) => {
                io.emit(C.ADD_CHAT, result);
            })
        })
    });

    /**
     * Make a Rain
     */
    client.on(C.RAIN, (data) => {
        let {token, amount, players, room, coin} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.makeRain(token, id, amount, coin, players, room, (result, rainStatus, error) => {
                client.emit(C.RAIN, encode(rainStatus));
                io.emit(C.ADD_CHAT, result);
            })
        })
    });

    /**
     * Add a Friend
     */
    client.on(C.MY_FRIENDS, (data) => {
        let {token} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.myFriends(id, (result) => {
                client.emit(C.MY_FRIENDS, encode(result));
            })
        })
    });

    /**
     * Add a Friend
     */
    client.on(C.ADD_FRIEND, (data) => {
        let {token, name} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.addFriend(id, name, (result) => {
                client.emit(C.ADD_FRIEND, encode(result));
            })
        })
    });

    /**
     * Send Tip
     */
    client.on(C.SEND_TIP, (data) => {
        let {token, target, amount, coin} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.sendTip(id, target, amount, coin, (result) => {
                io.emit(C.SEND_TIP, encode(result));
            })
        })
    });

    /**
     * Edit Account
     */
    client.on(C.EDIT_ACCOUNT, (data) => {
        let {token, email, username} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.editAccount(id, email, username, (result) => {
                client.emit(C.EDIT_ACCOUNT, encode(result));
            })
        })
    });

    /**
     * Edit Passwrod
     */
    client.on(C.EDIT_PASSWORD, (data) => {
        let {token, password} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.editPassword(id, password, (result) => {
                client.emit(C.EDIT_PASSWORD, encode(result));
            })
        })
    });

    /**
     * Wallet History
     */
    client.on(C.WALLET_HISTORY, (data) => {
        let {token} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.walletHistory(id, (result) => {
                client.emit(C.WALLET_HISTORY, encode(result));
            })
        })
    });

    /**
     * Submit New Withdrawal
     */
    client.on(C.SUBMIT_NEW_WITHDRAWL, (data) => {
        let {token, amount, coin} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.newWithdrawal(id, amount, coin, (result) => {
                client.emit(C.SUBMIT_NEW_WITHDRAWL, encode(result));
            })
        })
    });

    client.on(C.EARNINGS_WITHDRAW, (data) => {
        let {token, amount, coin, withdraw_to} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.earningsWithdrawal(id, amount, coin, withdraw_to, client, (result) => {
                client.emit(C.EARNINGS_WITHDRAW, encode(result));
            })
        })
    });

    client.on('BONUS_WITHDRAW', (data) => {
        let {token, amount, coin, withdraw_to} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.bonusWithdrawal(id, amount, coin, withdraw_to, client, (result) => {
                client.emit('BONUS_WITHDRAW', encode(result));
            })
        })
    });

    /**
     * Submit New Withdrawal
     */
    client.on(C.CREDIT_COIN, (data) => {
        let {token, coin} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.creditCoin(id, coin, (result) => {
                client.emit(C.CREDIT_COIN, encode(result));
            })
        })
    });

    /**
     * Get Wallet Address
     */
    client.on(C.GET_ADDRESS, (data) => {
        let {token, coin} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return
            Rule.getWalletAddress(id, coin, (result) => {
                client.emit(C.GET_ADDRESS, encode(result));
            })
        })
    });

    /**
     * BankRoll Amount
     */
    client.on(C.BANKROLL, (data) => {
        let {game, coin} = decode(data);
        Rule.getBankRoll(game, coin, (result) => {
            client.emit(C.BANKROLL, encode(result));
        })
    });

    /**
     * User Info
     */
    client.on(C.USER_INFO, (data) => {
        let {name, coin, first} = decode(data);
        Rule.userInfo(name, coin, first, (result) => {
            client.emit(C.USER_INFO, encode(result));
        })
    });

    /**
     * Game Details
     */
    client.on(C.GAME_DETAILS, (data) => {
        let {id} = decode(data);
        Rule.gameDetails(id, (result) => {
            client.emit(C.GAME_DETAILS, encode(result));
        })
    });

    /**
     * User Chart
     */
    client.on(C.USER_CHART, (data) => {
        let {name, game, coin} = decode(data);
        Rule.userChart(name, game, coin, (result) => {
            client.emit(C.USER_CHART, encode(result));
        })
    });

    /**
     * Messages
     */
    client.on(C.MESSAGES, (data) => {
        let {token, name} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.messages(token, id, name, (result) => {
                client.emit(C.MESSAGES, encode(result));
            })
        })
    });

    /**
     * Send a Private Messages
     */
    client.on(C.ADD_MESSAGES, (data) => {
        let {token, to, message} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.addMessages(token, id, to, message, (result) => {
                io.emit(C.ADD_MESSAGES, encode(result));
            })
        })
    });

    /**
     * Chats
     */
    client.on(C.CHATS, (data) => {
        let {country, id} = decode(data);
        Rule.getChats(country, id, (result) => {
            client.emit(C.CHATS, (result));
        })
    });

    /**
     * Notifications
     */
    client.on(C.NOTIFICATION, () => {
        Rule.notifications((result) => {
            client.emit(C.NOTIFICATION, encode(result));
        })
    });

    /**
     * Get Top Winner
     */
    client.on(C.TOP_WINNERS, () => {
        Rule.topWinners((result) => {
            client.emit(C.TOP_WINNERS, result);
        })
    });

    client.on(C.TOP_REFERRERS, () => {
        Rule.topReferrers(10, (result) => {
            client.emit(C.TOP_REFERRERS, result);
        })
    });

    /**
     * Get The Last Bets (for index)
     */
    client.on(C.LAST_BETS, () => {
        Rule.lastBets((result) => {
            client.emit(C.LAST_BETS, encode(result));
        })
    });

    /**
     * Get The Last Bets (for single game)
     */
    client.on(C.LAST_BETS_BY_GAME, (data) => {
        let {game} = decode(data);
        Rule.lastGameBets(game, (result) => {
            client.emit(C.LAST_BETS_BY_GAME, encode({history: result}));
        })
    });

    /**
     * Upload User Avatar
     */
    let uploader = new siofu();
    uploader.dir = "./uploads";
    uploader.maxFileSize = 102400 * 100;
    uploader.chunkSize = 102400 * 100;
    uploader.listen(client);
    uploader.on("start", function (event) {
        if (/\.exe$/.test(event.file.name)) {
            uploader.abort(event.file.id, client);
        }
    });
    uploader.on("saved", function (event) {
        Rule.uploadAvatar(client, event, uploader, (result) => {
            client.emit(C.SAVE_AVATAR, encode(result));
        })
    });

    //JACKPOT
    client.on('JACKPOTS', (data) => {
        let {token, game} = decode(data); //not needed
        Rule.getActiveJackpots((result) => {
            client.emit('JACKPOTS', encode({result}));
        });
    });

    client.on('JACKPOT_TOP_3', (data) => {
        let {jackpot_id} = decode(data); //not needed
        Rule.getJackpotTopThree(jackpot_id, (result) => {
            client.emit('JACKPOT_TOP_3', result);
        })
    });

    client.on('JACKPOT_MINE', (data) => {
        let {token, jackpot_id} = decode(data);
        Token.getID(token, (id) => {
            if (!id) return;
            Rule.getJackpotMine(id, jackpot_id, (result) => {
                client.emit('JACKPOT_MINE', result);
            })
        })
    });
}

module.exports = User;