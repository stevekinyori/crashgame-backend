const _ = require('lodash');
const H = require('../General/Helper');
const Model = require('../General/Model');

/*
 * Token Object
*/
const Token = {};

/*
 * Get User ID By Token
*/
Token.getID = function (token, callback) {
    if (!token) return;

    Model.query('SELECT uid FROM tokens WHERE key = $1', [token], function (err, results) {
        if (err) {
            console.log('ERROR on Token : 20', err);
            return callback(false, true);
        } else {
            if (results.rows.length !== 0) {
                if (!_.isUndefined(results.rows[0])) {
                    callback(results.rows[0].uid, false);
                } else {
                    callback(false, true)
                }
            } else {
                callback(false, true);
            }
        }
    });
}

Token.getUserIdByToken = function (token, callback) {
    if (!token) return;

    Model.query('SELECT u.user_id, u.ref_earnings FROM tokens t join users u on t.uid = u.id WHERE t.key = $1', [token], function (err, results) {
        if (err) {
            console.log('ERROR on Token : 20', err);
            return callback(false, false, true);
        } else {
            if (results.rows.length !== 0) {
                if (!_.isUndefined(results.rows[0])) {
                    callback(results.rows[0].user_id, results.rows[0].ref_earnings, false);
                } else {
                    callback(false, false, true)
                }
            } else {
                callback(false, false, true);
            }
        }
    });
}

/*
 * Create Token
*/
Token.create = function (token, uid, callback) {
    if (!token && !uid) return;

    uid = _.toNumber(uid);
    Model.query('INSERT INTO tokens(uid, key) VALUES($1, $2)', [uid, token], function (err, result) {
        if (err) {
            console.log('ERROR on UserRule : 268', err);
            return callback(null, true);
        } else {
            H.log('info', `Create Token ===> \n ID: ${uid} \n Token Key: ${token}`);
            callback(true, false);
        }
    });
}

/*
 * Refresh Token
*/
Token.refresh = function (id, callback) {
    if (!id) return;

    let uid = _.toNumber(id);
    let token = H.randomString(25);

    Model.query('UPDATE tokens SET key = $1 WHERE uid = $2', [token, uid], function (err, res) {
        if (err) {
            console.log('Error on Refreshing Token: 84', err);
            return callback(null);
        }
        callback(token)
    });
}

module.exports = Token;