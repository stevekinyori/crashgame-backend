const assert = require('assert');
const _ = require('lodash');
const util = require('util');
const axios = require('axios');
const validator = require('validator');
const qs = require('qs');
const md5 = require('md5');
const bcrypt = require('bcryptjs');
const wValidator = require('multicoin-address-validator');
const Token = require('./Token');
const Socket = require('../General/Socket');
const pg = require('../General/Model');
const H = require('../General/Helper');
const configs = require('../config');
const C = require("../General/Constant");
const {encode} = require("../General/Buffer");
const unirest = require("unirest");

const secretRecaptcha = configs.secretRecaptcha;
const BotsLimit = configs.BotsLimit;
const BALANCE = configs.BALANCE;
const WALLET = configs.WALLET;
const UPLOAD_URL = configs.avatarUrl;
const TOKEN_COIN = configs.COIN;

//Declare Rule Object
const Rule = {};

/*
 * Auth User On Site
 */
Rule.authentication = function (client, id, identifier, callback) {
    if (id) {
        id = _.toNumber(id);
        Socket.add(client, id);

        Rule.getUserInfo(id, (result, err) => {
            if (err) {
                console.log('error on authentication, USER RULE: 47');
                return callback({
                    status: false,
                    id: null
                });
            }
            callback({
                status: true,
                name: result.name,
                email: result.email,
                avatar: result.avatar,
                phone: result.phone,
                friends: result.friends,
                room: result.room,
                id: id,
                user_id: result.user_id,
                identifier: identifier,
                plays_today: result.plays_today,
                bonus_end_time: result.bonus_end_time
            });
        });
    } else {
        callback({
            status: false,
            id: null
        });
    }
}

/*
 * Register Client
 */
Rule.register = function (username, password, email, mobile, method, referrer_id, callback) {
    let {
        isAlphanumeric,
        isAscii,
        isEmail,
        isEmpty,
        isNumeric,
        isLength
    } = validator;
    let {
        escape,
        toBoolean,
        normalizeEmail,
        stripLow,
        trim
    } = validator;

    if (!username && !password && !mobile) return;

    let errors = [];

    if (isEmpty(username) || isEmpty(password) || isEmpty(mobile)) {
        errors.push('All fields must be filled.');
    } else {
        if (!isLength(username, {
            max: 16
        })) errors.push('Usernames cannot be longer than 16 characters.');
        if (!isLength(password, {
            min: 4
        })) errors.push('Passwords must be at least 4 characters long.');
        if (!isLength(password, {
            max: 64
        })) errors.push('Passwords cannot be longer than 64 characters long.');
        if (!isNumeric(mobile, {
            max: 20
        })) errors.push('Please Enter Correct phone number.');
    }

    // Check if the username has already been taken in the database
    Rule.getIdByName(username, (user_id) => {
        if (user_id) errors.push('Username already taken.');

        Rule.validateMobile(mobile, 'register', (err, phone) => {

            if (err) {
                errors.push(phone);
                return callback({
                    status: errors
                });
            }

            // Check if the phone has already been used in the database
            Rule.getIdByPhone(phone, (user_id) => {

                if (user_id) errors.push("The Phone Number ", phone, " is already taken.");

                // If there are no errors
                if (errors.length === 0) {
                    // Create a new user in the database
                    Rule.createUser({username, email, password, phone}, (create_user_id, err) => {
                        // If there was no error
                        if (!err) {
                            let user_id = _.toNumber(referrer_id);
                            Rule.getUserByUserId(user_id, (result, er) => {
                                if (er) {
                                    H.flog("[temp] referrer not found... just register")
                                } else {
                                    Rule.addHoldingRefferal(create_user_id, user_id, (ok) => {
                                        if (!ok) console.log('error on 145, referrer_id')
                                    })
                                }
                            })

                            //send sms
                            let msg = "Hi " + username + ", your account has been created.\n\nUsername: " + phone + "\nPassword: " + password + "\n\nRegards,\n" + configs.domain;
                            H.sendSms(phone, msg, configs.sender_name, configs.api_key);

                            return callback({
                                status: true,
                                uid: create_user_id,
                                name: username
                            })
                        } else {
                            console.log('Error on Rule: 106', err);
                            return callback({
                                status: 'Database Error'
                            })
                        }

                    });
                } else {
                    return callback({
                        status: errors
                    })
                }
            });
        });
    });
}

/*
 * Login Client
 */
Rule.login = function (mobile, password, recaptcha, token, callback) {
    let {
        escape,
        stripLow,
        trim
    } = validator;

    if (!mobile && !password && !recaptcha) return;

    //Validate Recaptcha
    axios({
        url: 'https://www.google.com/recaptcha/api/siteverify',
        method: 'POST',
        data: qs.stringify({
            secret: secretRecaptcha,
            response: recaptcha
        })
    }).then(function (req) {
        let response = req.data;

        //if Recaptch is Valid
        if (recaptcha === 'google' || response.success) {
            Rule.validateMobile(mobile, 'login', (err, phone) => {

                if (err) {
                    return callback({status: phone});
                }

                // Find a user in the database with the phone entered
                Rule.getIdByPhone(phone, (user_id) => {
                    if (user_id) {
                        // Find the password for the user from the database
                        Rule.getPasswordById(user_id, (hash) => {
                            // Compare the password entered to the hash from the database
                            Rule.comparePassword(password, hash, (match) => {
                                // If the password matches the hash
                                if (match) {
                                    // Login the user
                                    Rule.getUserInfo(user_id, (result, er) => {
                                        if (er) return;

                                        Token.create(token, user_id, (created) => {
                                            if (created) {
                                                callback({
                                                    status: true,
                                                    uid: user_id,
                                                    user_id: result.user_id,
                                                    token: token,
                                                    name: result.name,
                                                    email: result.email,
                                                    credit: result.balance,
                                                    friends: result.friends,
                                                    avatar: result.avatar
                                                })
                                            }
                                        });
                                    });
                                } else {
                                    return callback({
                                        status: 'Invalid password'
                                    });
                                }
                            });
                        });
                    } else {
                        return callback({
                            status: 'Account Not Found'
                        });
                    }
                });
            });
        } else {
            //Recaptch is inValid
            return callback({
                status: 'Invalid Captcha!'
            });
        }
    });
}

/*
 * Login Client by Google
 */
Rule.loginByGoogle = function (username, email, token, callback) {
    let {
        escape,
        stripLow,
        trim
    } = validator;
    if (!email && !username && !token) return;

    username = escape(stripLow(trim(username)));
    let password = md5(email); // default

    // Check If this user is exsits in database
    Rule.getIdByEmail(email, (user_id) => {
        // if User not exsits
        if (!user_id) {
            //Register User, Then login
            Rule.createUser({
                username,
                email,
                password
            }, (user, err) => {
                if (!err) {

                    Rule.getUserInfo(user, (result, er) => {
                        if (er) return;

                        Token.create(token, result.id, (created) => {
                            if (created) {
                                callback({
                                    status: true,
                                    token: token,
                                    name: result.name,
                                    email: result.email,
                                    credit: result.balance,
                                    friends: result.friends,
                                    avatar: result.avatar
                                })
                            }
                        });
                    });
                } else {
                    console.log('ERROR ON USER: 204', err);
                    return callback({
                        status: 'Database Error'
                    });
                }
            });
        }
        //Else, Login User
        else {
            Rule.getUserInfo(user_id, (result, er) => {
                if (er) return;

                Token.create(token, user_id, (created) => {
                    if (created) {
                        callback({
                            status: true,
                            token: token,
                            name: result.name,
                            email: result.email,
                            credit: result.balance,
                            friends: result.friends,
                            avatar: result.avatar
                        })
                    }
                });
            });
        }
    });
}

/*
 * Reset User Password
 */
Rule.resetClientPassword = function (mobile, callback) {
    Rule.validateMobile(mobile, 'pass', (err, phone) => {

        if (err) {
            return callback({status: false});
        }

        phone = data;
        Rule.getIdByPhone(phone, (user_id) => {
            if (user_id) {
                // Find the password for the user from the database
                Rule.getPassword2ById(user_id, (password) => {
                    if (!password) {
                        return callback({status: false});
                    }

                    //send sms
                    let msg = "Your account has been retrieved successfully.\n\nUsername: " + phone + "\nPassword: " + password + "\n\nRegards,\n" + configs.domain;
                    H.sendSms(phone, msg, configs.sender_name, configs.api_key);

                    return callback({status: true});

                })
            } else return callback({
                status: false
            });
        })
    })
}

Rule.createUser = function (data, callback) {
    let {username, email, password, phone} = data;
    if (!username && !password && !phone) return;

    let avatar = UPLOAD_URL + 'avatar.png';

    bcrypt.hash(password, 10, (err, hash) => {
        let id = _.toNumber(H.randomIntger(10)),
            friends = "Support",
            balance = JSON.stringify(BALANCE),
            wallet = JSON.stringify(WALLET),
            profit_high = JSON.stringify(BALANCE),
            profit_low = JSON.stringify(BALANCE),
            profit = JSON.stringify(BALANCE);

        pg.query('INSERT INTO users(name, email, password, phone, avatar, id, friends, balance, wallet, profit_high, profit_low, profit, password2) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)',
            [username, email, hash, phone, avatar, id, friends, balance, wallet, profit_high, profit_low, profit, password], function (err, result) {
                if (err) {
                    console.log('error on UserRule: 253', err)
                    return callback(false, true);
                } else callback(id, false);
            });
    });
}

/*
 * Add a new refferal
 */
Rule.addrefferal = function (uid, username, amount, callback) {
    if (!uid && username && amount) return console.log('-1');
    uid = _.toNumber(uid);

    pg.query(`INSERT INTO refferals (uid, earned, username)
              VALUES ($1, $2, $3)`, [uid, amount, username], function (err, ok) {
        if (!err) {
            callback(true);
        } else {
            callback(false)
        }
    })
}

/*
 * Add a new holding refferal
 */
Rule.addHoldingRefferal = function (create_user_id, referrer_user_id, callback) {
    if (!referrer_user_id && !create_user_id) return console.log('-1 485');
    referrer_user_id = _.toNumber(referrer_user_id);
    create_user_id = _.toNumber(create_user_id);

    pg.query(`UPDATE users SET referrer_id = $1 WHERE id = $2`, [referrer_user_id, create_user_id], function (err, ok) {
        if (!err) {
            callback(true);
        } else {
            callback(false)
        }
    })
}

/*
 * Get the target referrer_id
 */
Rule.targetRefferal = function (id, callback) {
    if (!id) return;
    id = _.toNumber(id);

    pg.query('SELECT referrer_id FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 508', err)
            return callback(null);
        }
        try {
            callback(results.rows[0].referrer_id);
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get the all referral list
 */
Rule.allReferral = function (user_id, callback) {
    //count
    let count_referrals = 0;
    pg.query("SELECT COUNT(*) from users where referrer_id = $1", [user_id], function (err, res) {
        if (err) {
            console.log('error on UserRule: 500', err);
        } else {
            count_referrals = res.rows[0].count;
        }

        //latest list
        pg.query("select id, created, name as username, '0' as earned from users where referrer_id = $1 ORDER by created DESC limit 20", [user_id], function (err, results) {
            if (err) {
                console.log('error on UserRule: 493', err)
                return callback(false, false);
            }
            try {
                callback(results.rows, count_referrals);
            } catch (e) {
                console.log(e);
            }
        });
    });
}

Rule.earningsTrx = function (uid, callback) {
    pg.query("select e.*, u.name from ref_earnings e join users u on e.from_uid = u.id where e.to_uid = $1 ORDER by e.created DESC limit 20", [uid], function (err, results) {
        if (err) {
            console.log('error on UserRule: 513', err)
            return callback(false, false);
        }
        try {
            callback(results.rows);
        } catch (e) {
            console.log(e)
        }
    });
}

Rule.happyHourToday = function (id, callback) {
    let earnings_today = 0;
    let total = 0;
    let bonus_end_time = 0;

    Rule.getUserInfo(id, (result, err) => {
        if (err) return callback(earnings_today, total, bonus_end_time);

        bonus_end_time = result.bonus_end_time;

        pg.query("select count(*) count, sum(bonus) earned from bonus_earnings where uid = $1 and date(created) = current_date", [id], function (err, res) {
            if (err) {
                return callback(earnings_today, total, bonus_end_time);
            } else {
                earnings_today = res.rows[0].earned;

                pg.query("select count(*) count, sum(bonus) total_earned from bonus_earnings where uid = $1", [id], function (err_, res_) {
                    if (err_) {
                        return callback(earnings_today, total, bonus_end_time);
                    } else {
                        total = res_.rows[0].total_earned;

                        return callback(earnings_today, total, bonus_end_time)
                    }
                });
            }
        });
    });
}

Rule.topHappyHour = function (limit, callback) {
    pg.query("select e.uid, u.name, u.user_id, sum(e.bonus)::numeric total_earnings from bonus_earnings e left join users u on e.uid = u.id group by e.uid, u.name, u.user_id order by total_earnings DESC limit $1", [limit], function (err, results) {
        if (err) {
            console.log('error on UserRule: 1952', err)
            return callback(false);
        }
        try {
            callback(results.rows);
        } catch (e) {
            console.log('Error on UserRule 1958: ' + e);
        }
    });
}

/*
 * Get Client Bets
 * used in games page in "my bets" tab
 */
Rule.getClientBets = function (uid, game, callback) {
    if (!uid && !game) return;
    uid = _.toNumber(uid);

    pg.query("SELECT * FROM bets WHERE uid = $1 AND game = $2 ORDER by created DESC LIMIT 20", [uid, 'crash'], function (err, results) {
        if (err) {
            console.log('error on UserRule: 514', err)
            return callback(false, true);
        }
        try {
            callback(_.reverse(results.rows), false);
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Client History
 * used in games wallet history modal
 */
Rule.getClientHistory = function (uid, callback) {
    if (!uid) return;

    uid = _.toNumber(uid);
    pg.query("SELECT * FROM bets WHERE uid = $1 ORDER by created DESC LIMIT 10", [uid], function (err, results) { //Dan - Limit 20
        if (err) {
            console.log('error on UserRule: 384', err)
            return callback(false, true);
        }
        try {
            callback(_.reverse(results.rows), false);
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Client ID By Name
 */
Rule.getIdByName = function (name, callback) {
    if (!name) return;

    pg.query('SELECT id FROM users WHERE name = $1', [name], function (err, results) {
        if (err) {
            console.log('error on UserRule: 60:', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].id, false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
}

Rule.validateMobile = function (phone, instance, callback) {
    let req = unirest('GET', 'https://dev.vashub.co.ke/api/lookup?return=plain&mobile=' + encodeURIComponent(phone))
        .timeout(20000)
        .end(function (res) {
            if (res.error) {
                console.log('validate mobile [' + instance + ']: ' + phone + ' >> error: ' + res.error);
                return callback(true, 'Error occurred! Try later!');
            } else {
                console.log('validate mobile [' + instance + ']: ' + phone + ' >> ' + res.raw_body);
                if (res.raw_body === '0') {
                    callback(true, "Mobile '" + phone + "' is invalid!");
                } else {
                    callback(false, res.raw_body);
                }
            }
        });
}

/*
 * Get Client ID By Phone
 */
Rule.getIdByPhone = function (phone, callback) {
    if (!phone) return;

    pg.query('SELECT id FROM users WHERE phone = $1', [phone], function (err, results) {
        if (err) {
            console.log('error on UserRule: 60')
            return callback(false);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].id);
            else
                callback(false)
        } catch (e) {
            console.log(e)
        }
    });
}

Rule.getUserByPhone = function (phone, callback) {
    if (!phone) return;

    pg.query('SELECT * FROM users WHERE phone = $1', [phone], function (err, results) {
        if (err) {
            console.log('error on UserRule: 777')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0])) {
                if (results.rows[0].length === 0)
                    return callback(false, true)
                else
                    callback(results.rows[0], false);
            } else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Get Client ID By Phone
 */
Rule.getPhoneById = function (id, callback) {
    if (!id) return;

    pg.query('SELECT phone FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 531')
            return callback(false);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].phone);
            else
                callback(false)
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Client Name By Id
 */
Rule.getNameById = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);
    pg.query('SELECT name FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 157', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].name, false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Client ID By Email
 */
Rule.getIdByEmail = function (email, callback) {
    if (!email) return;

    pg.query('SELECT id FROM users WHERE email = $1', [email], function (err, results) {
        if (err) {
            console.log('error on UserRule: 83', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].id, false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Client Password By ID
 */
Rule.getPasswordById = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);
    pg.query('SELECT password FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 391', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].password, false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Client Password 2 By ID [ Forget Password ]
 */
Rule.getPassword2ById = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);
    pg.query('SELECT password2 FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 391', err)
            return callback(false);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].password2);
            else
                callback(false)
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get User Info by ID
 */
Rule.getUserInfo = function (id, callback) {
    if (!id) return;
    id = _.toNumber(id);

    pg.query('SELECT * FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 566')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0])) {
                if (results.rows[0].length === 0)
                    return callback(false, true)
                else
                    callback(results.rows[0], false);
            } else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

Rule.getUserByUserId = function (user_id, callback) {
    if (!user_id) return;
    user_id = _.toNumber(user_id);

    pg.query('SELECT * FROM users WHERE user_id = $1', [user_id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 792')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0])) {
                if (results.rows[0].length === 0)
                    return callback(false, true)
                else
                    callback(results.rows[0], false);
            } else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Get User Info by ID method 2
 */
Rule.getUserOrBot = function (id, botinfo = null, callback) {
    if (!id) return;

    //if is bot
    if (botinfo !== null) {
        return callback(botinfo, false);
    }

    id = _.toNumber(id);
    pg.query('SELECT * FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 149')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0], false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Get User Info by Name
 */
Rule.getUserInfoByName = function (name, callback) {
    if (!name) return;

    pg.query('SELECT * FROM users WHERE name = $1', [name], function (err, results) {
        if (err) {
            console.log('error on UserRule: 239')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0], false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Get User Info by Email
 */
Rule.getUserInfoByEmail = function (email, callback) {
    if (!email) return;

    pg.query('SELECT * FROM users WHERE email = $1', [email], function (err, results) {
        if (err) {
            console.log('error on UserRule: 260')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0], false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Compare Password
 */
Rule.comparePassword = function (password, hash, callback) {
    if (!password) return;

    // Compare password to hash using bcrypt
    bcrypt.compare(password, hash, (err, res) => {
        // If no error and match found, send true
        if (!err, res) {
            callback(true);
            // If error or no match found, send false
        } else {
            callback(false);
        }
    });
};

/*
 * Get Client Credit
 */
Rule.getClientCredit = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);
    pg.query('SELECT balance FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 493', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0]))
                callback(results.rows[0].balance, false);
            else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Get Client Credit by Coin
 */
Rule.getClientCoinCredit = function (id, coin, callback) {
    if (!id && !coin) return callback(false, true);

    id = _.toNumber(id);
    coin = _.lowerCase(coin);

    assert(id && coin);

    pg.query('SELECT balance, status FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 516', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0])) {

                let bc = results.rows[0].balance[coin];
                let user_status = results.rows[0].status;

                if (_.isUndefined(bc))
                    bc = '0.00000000';

                let credit = _.toString(bc);
                credit = _.replace(credit, ",", ".");
                credit = _.toNumber(credit); // Changed
                credit = H.CryptoSet(credit, coin);
                callback(credit, false, user_status);
            } else {
                return callback(false, true);
            }
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Get Chats
 */
Rule.getChatsXXXX = function (country, id, callback) {
    if (!country) return;

    country = _.lowerCase(country);
    let table = 'chat_' + country;

    let allowed_all = [6252686165/*Support*/, 8373372189/*Admin*/, 4153751612/*Dan*/, 4456135/*777*/, 2845885642/*Mamboleo*/];

    let query = `SELECT * FROM ${table} where level = '1' ORDER BY ${table}.sorter DESC LIMIT 20`;

    if (!allowed_all.includes(id)) {
        query = `SELECT * FROM ${table} where level = '1' and uid in('6252686165', '` + id + `') ORDER BY ${table}.sorter DESC LIMIT 20`;
    }

    pg.query(query, function (err, results) {
        if (err) {
            console.log('error USER Role: 1003', err);
            return callback(false, true);
        }
        try {
            callback(_.reverse(results.rows), false);
        } catch (e) {
            console.log(e)
        }
    });
};

Rule.getChats = function (country, id, callback) {
    pg.query(`SELECT * FROM chat_global where show = $1 ORDER BY id DESC LIMIT $2`, [true, 20], function (err, results) {
        if (err) {
            console.log('error USER Role: 1003', err);
            return callback(false, true);
        }
        try {
            callback(_.reverse(results.rows), false);
        } catch (e) {
            console.log(e)
        }
    });
};

/*
 * Add a Chat
 */
Rule.addChat = function (id, country, message, io, callback) {

    if (!id && !message) return;

    if (message === "") return;
    if (message === " ") return;

    if (_.isString(message)) {
        if (_.isUndefined(message)) return;
        if (message.trim() === "") return;
        if (message.trim() === " ") return;
    }

    //hash bad keywords
    message = message.replace(/pakakumi/gi, '****').replace(/fuck/gi, '***').replace(/paka/gi, '***').replace(/kuma/gi, '***').replace(/betkumi/gi, '***').replace(/pesacrash/gi, '***');

    id = _.toNumber(id);

    pg.query('SELECT * FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 270', err)
            return callback(false, true);
        }

        if (_.isUndefined(results.rows[0])) return;

        let time = H.getCurrentTime(new Date());
        let sorter = _.toNumber(new Date().getTime());
        let name = results.rows[0].name;
        let avatar = results.rows[0].avatar;
        let muted = results.rows[0].muted;
        let level = results.rows[0].level;
        let plays = results.rows[0].plays;
        let phone = results.rows[0].phone;
        let chat_status = results.rows[0].chat_status;

        let show = true;
        callback({country: country.toLowerCase(), message: message, name: name, id: id, avatar: avatar, time: time, date: H.getCurrentDate(new Date()), sorter: sorter, level: level}, false);

        /*let show = false;
        if (chat_status === 'Allowed') {
            show = true;
            callback({country: country.toLowerCase(), message: message, name: name, id: id, avatar: avatar, time: time, date: H.getCurrentDate(new Date()), sorter: sorter, level: level}, false);
        } else {
            callback(false, true);
        }*/

        pg.query(`INSERT INTO chat_global(name, uid, avatar, message, time, sorter, level, show) VALUES($1, $2, $3, $4, $5, $6, $7, $8)`, [name, id, avatar, message, time, sorter, level, show], function (err, result) {
            if (err) {
                console.log('error on UserRule: 910', err)
                return callback(false, true);
            }
        });
    });
};

Rule.totalDeposits = function (id, callback) {
    if (!id) return callback(0, false);

    id = _.toNumber(id);

    pg.query('select sum(amount) spent from deposits where uid = $1', [id], function (err, results) {
        if (err) return callback(0, false);

        if (results.rows.length === 0) {
            return callback(0, false);
        }

        callback(results.rows[0].spent, false);
    });
}

/*
 * Get Friend List
 */
Rule.getFriends = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);
    pg.query('SELECT friends FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('User Role Error: 309', err);
            return callback(false, true);
        }
        callback(results.rows[0].friends, false);
    });
};

/*
 * Add A Friend
 */
Rule.addFriend = function (id, name, callback) {
    if (!id && !name) return;

    id = _.toNumber(id);
    Rule.getFriends(id, (result) => {
        let status;

        let list;

        let friends = _.split(result, ',');

        if (friends.includes(name)) {
            //Remove From Friends
            list = _.replace(result, name, '');
            status = false;
        } else {
            //Add to Friends
            list = result + ',' + name;
            status = true;
        }

        pg.query('UPDATE users SET friends = $1 WHERE id = $2', [list, id], function (err, res) {
            if (err) {
                console.log('User Role Error: 611', err);
                return callback(false, true);
            }
            callback({
                name: name,
                data: list,
                status: status
            }, false)
        });
    });
}

/*
 * Reduce Client Balance
 */
Rule.reduceBalance = function (id, amount, coin, instance = '', callback) {
    if (!id && !amount && !coin) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);

    Rule.getUserInfo(id, (result, er) => {
        if (er) return;

        if (_.isUndefined(result)) return;
        if (result.length === 0) return;

        let plays = (instance === 'bet') ? (_.toNumber(result.plays) + 1) : result.plays;
        let balance = result.balance;
        let plays_today = result.plays_today;

        if (plays_today <= 0) {
            pg.query(`UPDATE users SET bonus_end_time = (now() + INTERVAL '1 hour') WHERE id = $1`, [id], function (err, ok) {
            });
        }

        let bc = balance[coin];

        if (_.isUndefined(bc))
            bc = '0.00';

        let r = _.toNumber(bc) - _.toNumber(amount);
        let reduce = H.CryptoSet(r, coin);
        let update = Object.assign({}, balance, {
            [coin]: reduce
        });
        update = JSON.stringify(update);

        pg.query(`UPDATE users SET balance = $1, plays = $2, plays_today = plays_today + '1' WHERE id = $3`, [update, plays, id], function (err, ok) {
            if (ok) {
                callback(reduce, false);
            } else
                callback(false, true)
        });
    })
};

/*
 * Add Client Balance
 */
Rule.addBalance = function (id, amount, new_bonus, coin, callback) {
    if (!id && !coin && !amount) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);
    let bonus_key = 'bonus';

    //H.flog("add user balance: id: " + id + " | amt: " + amount + " | coin: " + coin);

    Rule.getUserInfo(id, (result, error) => {
        if (error) return;

        let balance = result.balance;

        let bc = balance[coin];
        let bonus = balance[bonus_key];

        if (_.isUndefined(bc)) {
            bc = '0';
        }
        if (_.isUndefined(bonus)) {
            bonus = '0';
        }

        let plus = _.toNumber(bc) + _.toNumber(amount);
        let plus_bonus = (_.toNumber(bonus) + _.toNumber(new_bonus)).toFixed(2);

        if (_.isNaN(plus) || _.isNaN(plus_bonus)) {
            H.flog("error: add user balance: id: " + id + " | bal: " + bc + " | amt: " + amount + " | bonus: " + bonus + " >> n.bal: " + plus + " | n.bonus: " + plus_bonus);
            return callback(false, 0, true);
        }

        let update = Object.assign({}, balance, {
            [coin]: plus, [bonus_key]: plus_bonus
        });
        update = JSON.stringify(update);

        pg.query(`UPDATE users SET balance = $1 WHERE id = $2`, [update, id], function (err, ok) {
            if (ok) {
                callback(plus, plus_bonus, false);
            } else {
                callback(false, 0, true)
            }
        })
    })
};

Rule.saveBonus = function (uid, bet_amount, new_bonus, bonus_rate, source, callback) {
    if (!uid && new_bonus && bet_amount) return console.log('error; save bonus failed!');
    uid = _.toNumber(uid);

    pg.query(`INSERT INTO bonus_earnings (uid, bet_amount, bonus, bonus_rate, source) VALUES ($1, $2, $3, $4, $5)`, [uid, bet_amount, new_bonus, bonus_rate, source], function (err, ok) {
        if (!err) {
            callback(true);
        } else {
            callback(false)
        }
    })
}

/*
 * Add Amount to BankRoll
 */
Rule.addBankRoll = function (client, amount, coin, isB, callback) {
    // if is bot
    if (isB) {
        return callback(true, false);
    }

    pg.query(`UPDATE bankroll SET amount = round(amount + $1, 2) WHERE game = $2`, [_.toNumber(amount), 'crash'], function (err, ok) {
        callback(true, false);
    })
};

/*
 * Reduce Amount From BankRoll
 */
Rule.reduceBankRollXXX = function (amount, coin, callback) {
    pg.query('SELECT amount FROM bankroll WHERE game = $1', ['crash'], function (err, results) {
        if (err) {
            console.log('error on User Rule: 799', err)
            return callback(false, true);
        }

        let plusAmount = _.toNumber(results.rows[0].amount) - _.toNumber(amount);

        if (plusAmount > 0) {
            pg.query(`UPDATE bankroll SET amount = round($1, 2) WHERE game = $2`, [_.toNumber(plusAmount), 'crash'], function (err, ok) {
                callback(true, false);
            })
        }
    });
};

Rule.reduceBankRoll = function (client, amount, coin, isB, callback) {
    // if is bot
    if (isB) {
        return callback(true, false);
    }

    pg.query(`UPDATE bankroll SET amount = round(amount - $1, 2) WHERE game = $2`, [_.toNumber(amount), 'crash'], function (err, ok) {
        callback(true, false);
    })
};

Rule.updateUserNames = function (uid, full_name) {
    pg.query(`UPDATE users SET full_name = $1 WHERE id = $2`, [full_name, uid], function (err, ok) {
        if (err) {
            console.log('Error User Rule: 1234: ' + err)
        }
    });
}

/*
 * Update User Profit
 */
Rule.updateProfit = function (winner, id, amount, coin, callback) {
    if (!id && !coin) return;

    if (_.isUndefined(amount)) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);

    Rule.getUserInfo(id, (result, err) => {
        if (err) return;

        const profit = result.profit;
        const profitHigh = result.profit_high;
        const profitLow = result.profit_low;

        let pfHigh = _.toNumber(profitHigh[coin]);
        let pfLow = _.toNumber(profitLow[coin]);

        if (_.isUndefined(pfHigh)) return;
        if (_.isUndefined(pfLow)) return;

        let change;

        if (winner) { // is winner
            change = _.toNumber(profit[coin]) + _.toNumber(amount);
        } else {
            change = _.toNumber(profit[coin]) - _.toNumber(amount);
        }

        //Update High Profit
        if (change > pfHigh) {
            let update = Object.assign({}, profitHigh, {
                [coin]: H.CryptoSet(change, coin)
            });
            update = JSON.stringify(update);
            pg.query(`UPDATE users SET profit_high = $1 WHERE id = $2`, [update, id], function (err, ok) {
                if (err) {
                    console.log('Error User Rule: 802', err)
                }
            })
        }

        //Update Low Profit
        if (pfLow > change) {
            let update = Object.assign({}, profitLow, {
                [coin]: H.CryptoSet(change, coin)
            });
            update = JSON.stringify(update);
            pg.query(`UPDATE users SET profit_low = $1 WHERE id = $2`, [update, id], function (err, ok) {
                if (err) {
                    console.log('Error User Rule: 813', err)
                }
            })
        }

        //Update Total Profit
        let update = Object.assign({}, profit, {
            [coin]: H.CryptoSet(change, coin)
        });
        update = JSON.stringify(update);

        pg.query(`UPDATE users SET profit = $1 WHERE id = $2`, [update, id], function (err, ok) {
            if (ok) {
                callback(true, false);
            } else {
                callback(false, true);
            }
        })
    })
}

/*
 * Send Tip
 */
Rule.sendTip = function (id, target, amount, coin, callback) {
    if (!id && !target && !amount && !coin) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);
    amount = _.toNumber(amount);

    assert(id && amount);

    assert(amount > 0)

    Rule.getIdByName(target, (targetID) => {
        if (!targetID) return;

        //Check Sender Balance
        Rule.getClientCoinCredit(id, coin, (senderBalance, error) => {
            if (senderBalance === false) return;

            if (error) return;

            senderBalance = _.toNumber(senderBalance);

            if (senderBalance > amount && amount !== 0) {

                assert(senderBalance > amount);

                //Reduce Sender Balance
                Rule.reduceBalance(id, amount, coin, 'tip', (res, error) => {
                    if (error) return;

                    //Increase Reciever Balance
                    Rule.addBalance(targetID, amount, 0, coin, (result, newBonus, err) => {
                        if (err) return;

                        Rule.getNameById(id, (senderName) => {
                            if (!senderName) return;

                            callback({
                                    status: true,
                                    msg: 'Your tip was sent.',
                                    amount: amount,
                                    target: targetID,
                                    coin: coin,
                                    sender: _.toNumber(id),
                                    senderName: senderName
                                },
                                false);
                        })
                    });
                });
            } else {
                Rule.getNameById(id, (senderName) => {
                    callback({
                        status: false,
                        msg: 'Your credit is not enough',
                        senderName: senderName
                    }, false);
                })
            }
        })
    });
};

/*
 * Edit Client Account
 * When client want to edit setting
 */
Rule.editAccount = function (id, email, username, callback) {
    let {
        isEmail
    } = validator;
    if (!id) return;

    id = _.toNumber(id);

    if (!_.isUndefined(username)) {
        if (username.length > 1) {
            callback({
                status: false,
                error: 'Username is already taken.'
            })
        }

        Rule.getUserInfoByName(username, function (result) {
            result = _.toArray(result);

            if (result.length > 1) {
                callback({
                    status: false,
                    error: 'Username is already taken.'
                })
            } else {
                pg.query('UPDATE users SET name = $1 WHERE id = $2', [username, id], function (err, results) {
                    if (err) {
                        return callback({
                            status: false,
                            error: 'Something is Wrong !'
                        })
                    }

                    Rule.bulkChange(username, id, (changed) => {
                        if (changed) {
                            callback({
                                status: true
                            })
                        }
                    });
                });
            }
        });
    }
};

/*
 * Edit Client Password
 */
Rule.editPassword = function (id, password, callback) {
    if (!id && !password) return;

    id = _.toNumber(id);

    if (password.length < 6) {
        return callback({
            status: false,
            error: 'Password must be more than 4 characters!'
        });
    }

    bcrypt.hash(password, 10, (err, hash) => {
        pg.query('UPDATE users SET password = $1 WHERE id = $2', [hash, id], function (err) {
            if (err) return callback(err);

            pg.query('UPDATE users SET password2 = $1 WHERE id = $2', [password, id], function (err) {
                if (err) return callback(err);
                callback({status: true});
            });

        });
    });
}

/*
 * Client Wallet History
 * Get deposit / withdrawal history
 * TODO: exchanges
 */
Rule.walletHistory = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);

    pg.query('SELECT * FROM withdrawals WHERE uid = $1', [id], function (err, withdrawls) {
        if (err) return callback(err);

        pg.query('SELECT * FROM deposits WHERE uid = $1', [id], function (err, deposits) {
            if (err) return callback(err);

            callback({
                withdrawl: withdrawls.rows,
                deposit: deposits.rows,
                exchanges: []
            })
        });
    });
}

/*
 * Client Friends
 */
Rule.myFriends = function (id, callback) {
    if (!id) return;

    id = _.toNumber(id);

    pg.query('SELECT friends FROM users WHERE id = $1', [id], function (err, results) {
        if (err) return callback(err);

        if (results.rows.length === 0)
            return callback(false);

        callback(results.rows[0].friends);
    });
}

Rule.earningsWithdrawal = function (id, with_amount, coin, withdraw_to, client, callback) {
    if (!id && !coin && !with_amount) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);
    with_amount = _.toNumber(with_amount);

    H.flog("earnings - [" + id + "] amt: " + with_amount + " | to: " + withdraw_to + " (" + coin + ")");

    if (!H.isValidNumber(with_amount)) {
        return callback({status: 'Please enter a valid amount!', reason: 'error'});
    }

    if (with_amount < 100 && withdraw_to === 'M-PESA') {
        return callback({status: 'Minimum amount for M-PESA is 100/-', reason: 'error'});
    }

    Rule.getUserInfo(id, (result, error) => {
        if (error) return callback({status: 'Error getting your details!', reason: 'error'});

        let phone = result.phone;

        //balance
        let user_balance = result.balance;
        let user_bc = user_balance[coin];
        let ref_earnings = result.ref_earnings;

        //validations
        if (_.isNaN(user_bc) || _.isNaN(ref_earnings) || _.isNaN(with_amount)) {
            return callback({status: 'Error validating details. Try again!', reason: 'error'});
        }

        let cash = _.toNumber(user_bc);
        let earnings = _.toNumber(ref_earnings);

        if (with_amount > earnings) {
            return callback({status: 'Insufficient balance of KES ' + earnings + ' to withdraw KES ' + with_amount, reason: 'error'});
        }

        H.flog("earnings - [" + id + "] amt: " + with_amount + " | to: " + withdraw_to + " (" + coin + ") >> all good; earnings: " + ref_earnings + " | bal: " + cash);

        //all good, deduct ref_earnings
        pg.query('update users set ref_earnings = round(ref_earnings - $1, 2) WHERE id = $2', [with_amount, id], function (err, results) {
            if (err) {
                console.log('error on UserRule: 1615', err);
                return callback({status: 'System error. Try again!', reason: 'error'});
            } else {
                switch (withdraw_to) {
                    case 'M-PESA':
                        with_amount = Math.floor(with_amount);

                        let unique_reference = Math.floor(new Date().getTime() / 10000);

                        pg.query('INSERT INTO withdrawals(uid, amount, wallet, status, coin, charges, amount_sent, sys_safe) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id', [id, with_amount, phone, 'Created', coin, '0', with_amount, unique_reference], function (err, res) {
                            if (err) {
                                H.flog('b2c earnings [' + phone + ' | ' + with_amount + ' | bal: ' + cash + ' | id: ' + unique_reference + '] | failed >> duplicate: ' + err.message, 'error');
                                return callback({status: 'Duplicate request. Try again!', reason: 'error'});
                            } else {

                                //m-pesa
                                let withdrawal_id = res.rows[0].id;
                                let client_reference = id + 'x' + withdrawal_id;
                                let amount_to_send = with_amount;

                                H.doB2C(phone, amount_to_send, client_reference, (err, request_id) => {
                                    if (err) {
                                        H.flog('b2c earnings [' + phone + ' | ' + with_amount + ' [' + amount_to_send + '] | bal: ' + cash + ' | id: ' + withdrawal_id + '] | failed >> ' + request_id, 'error');

                                        pg.query("update withdrawals set status = $1 where id = $2", ['Pending', withdrawal_id], function (err, res) {
                                        });

                                        callback({status: 'Withdrawal of KES ' + with_amount + ' pending!'});
                                    } else {
                                        H.flog('b2c earnings [' + phone + ' | ' + with_amount + ' [' + amount_to_send + '] | bal: ' + cash + ' | id: ' + withdrawal_id + '] | success >> id: ' + request_id, 'notice');

                                        pg.query("update withdrawals set status = $1, request_id = $2 where id = $3", ['Sent', request_id, withdrawal_id], function (err, res) {
                                        });

                                        callback({status: 'Withdrawal of KES ' + with_amount + ' scheduled successfully.'});
                                    }
                                });
                            }
                        });
                        break;
                    case 'Wallet':
                        Rule.addBalance(id, with_amount, 0, coin, (result, newBonus, err) => {
                            console.log(H.getLogTime() + " | earnings wallet - [" + id + "] amt: " + with_amount + " | to: " + withdraw_to + " (" + coin + ") >> all good; earnings: " + ref_earnings + " | bal: " + cash + " | add balance >>", result);
                        });

                        return callback({status: 'Processed successfully.', reason: 'success'});

                        break;
                    default:
                        return callback({status: 'An error occurred. Try again!', reason: 'error'});
                        break;
                }
            }
        });
    });
}

Rule.bonusWithdrawal = function (id, with_amount, coin, withdraw_to, client, callback) {
    if (!id && !coin && !with_amount) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);
    with_amount = _.toNumber(with_amount);

    H.flog("bonus withdraw - [" + id + "] amt: " + with_amount + " | to: " + withdraw_to + " (" + coin + ")");

    if (!H.isValidNumber(with_amount)) {
        return callback({status: 'Please enter a valid amount!', reason: 'error'});
    }

    if (with_amount < 100 && withdraw_to === 'M-PESA') {
        return callback({status: 'Minimum amount for M-PESA is 100/-', reason: 'error'});
    }

    Rule.getUserInfo(id, (result, error) => {
        if (error) return callback({status: 'Error getting your details!', reason: 'error'});

        let phone = result.phone;

        //balance
        let user_balance = result.balance;
        let user_bc = user_balance[coin];
        let user_bonus = user_balance['bonus'];

        //validations
        if (_.isNaN(user_bc) || _.isNaN(user_bonus) || _.isNaN(with_amount)) {
            return callback({status: 'Error validating details. Try again!', reason: 'error'});
        }

        let cash = _.toNumber(user_bc);
        let earnings = _.toNumber(user_bonus);

        if (with_amount > earnings) {
            return callback({status: 'Insufficient balance of KES ' + earnings + ' to withdraw KES ' + with_amount, reason: 'error'});
        }

        H.flog("bonus - [" + id + "] amt: " + with_amount + " | to: " + withdraw_to + " (" + coin + ") >> all good; bonus: " + earnings + " | bal: " + cash);

        //all good, deduct bonus
        pg.query(`update users set balance = jsonb_set(balance::jsonb, '{bonus}', (((balance->>'bonus')::numeric - $1)::text)::jsonb, false) WHERE id = $2`, [with_amount, id], function (err, results) {
            if (err) {
                console.log('error on UserRule: 1809', err);
                return callback({status: 'System update error. Try again!', reason: 'error'});
            } else {
                switch (withdraw_to) {
                    case 'M-PESA':
                        with_amount = Math.floor(with_amount);

                        let unique_reference = Math.floor(new Date().getTime() / 10000);

                        pg.query('INSERT INTO withdrawals(uid, amount, wallet, status, coin, charges, amount_sent, sys_safe) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id', [id, with_amount, phone, 'Created', coin, '0', with_amount, unique_reference], function (err, res) {
                            if (err) {
                                H.flog('b2c earnings [' + phone + ' | ' + with_amount + ' | bal: ' + cash + ' | id: ' + unique_reference + '] | failed >> duplicate: ' + err.message, 'error');
                                return callback({status: 'Duplicate request. Try again!', reason: 'error'});
                            } else {

                                //m-pesa
                                let withdrawal_id = res.rows[0].id;
                                let client_reference = id + 'x' + withdrawal_id;
                                let amount_to_send = with_amount;

                                H.doB2C(phone, amount_to_send, client_reference, (err, request_id) => {
                                    if (err) {
                                        H.flog('b2c bonus [' + phone + ' | ' + with_amount + ' [' + amount_to_send + '] | bal: ' + cash + ' | id: ' + withdrawal_id + '] | failed >> ' + request_id, 'error');

                                        pg.query("update withdrawals set status = $1 where id = $2", ['Pending', withdrawal_id], function (err, res) {
                                        });

                                        callback({status: 'Withdrawal of KES ' + with_amount + ' pending!'});
                                    } else {
                                        H.flog('b2c bonus [' + phone + ' | ' + with_amount + ' [' + amount_to_send + '] | bal: ' + cash + ' | id: ' + withdrawal_id + '] | success >> id: ' + request_id, 'notice');

                                        pg.query("update withdrawals set status = $1, request_id = $2 where id = $3", ['Sent', request_id, withdrawal_id], function (err, res) {
                                        });

                                        callback({status: 'Withdrawal of KES ' + with_amount + ' scheduled successfully.'});
                                    }
                                });
                            }
                        });
                        break;
                    case 'Wallet':
                        Rule.addBalance(id, with_amount, 0, coin, (result, newBonus, err) => {
                            console.log(H.getLogTime() + " | bonus to wallet - [" + id + "] amt: " + with_amount + " | to: " + withdraw_to + " (" + coin + ") >> all good; bonus: " + earnings + " | bal: " + cash + " | add balance >>", result);
                        });

                        return callback({status: 'Processed successfully.', reason: 'success'});

                    default:
                        return callback({status: 'An error occurred. Try again!', reason: 'error'});
                }
            }
        });
    });
}

/*
 * Submit New Withdrawal
 */
Rule.newWithdrawal = function (id, with_amount, coin, callback) {
    if (!id && !coin && !with_amount) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);
    with_amount = _.toNumber(with_amount);

    if (!H.isValidNumber(with_amount)) {
        return callback({
            status: 'Please enter a valid amount!'
        });
    }

    if (with_amount < configs.MINIMUM_WITHDRAW)
        return callback({status: 'Minimum amount for withdrawal is 10/-'});

    if (with_amount > 140000)
        return callback({status: 'Maximum amount for withdrawal is 140,000/-'});

    // Get the user Credit
    //Rule.getClientCoinCredit(id, coin, (result) => {
    Rule.getUserInfo(id, (result, error) => {
        if (error) return callback({status: 'Error -3'});

        let phone = result.phone;

        //balance
        let user_balance = result.balance;
        let user_bc = user_balance[coin];
        let bonus = user_balance['bonus'];
        let user_status = result.status;

        if (user_status === 'Suspended') {
            H.flog('b2c [' + phone + ' | ' + with_amount + ' | bal: ' + user_bc + '] >> user suspended');
            return callback({status: 'Network error. Try after some time.'});
        }

        if (_.isUndefined(user_bc)) {
            H.flog('b2c [' + phone + ' | ' + with_amount + ' | bal: ' + user_bc + '] >> user balance is undefined');
            return callback({
                status: 'There was an error withdrawing KES ' + with_amount + '. Try after some time.'
            });
        }

        if (_.isNaN(user_bc)) {
            H.flog('b2c [' + phone + ' | ' + with_amount + ' | bal: ' + user_bc + '] >> user balance not a number');
            return callback({status: 'There was an error withdrawing KES ' + with_amount + '. Try after some time.'});
        }

        let cash = _.toNumber(user_bc);

        H.flog('b2c [' + id + ' | ' + phone + ' | ' + with_amount + ' | bal: ' + cash + '] bonus: ' + bonus);

        if (cash < with_amount) {
            H.flog('b2c [' + phone + ' | ' + with_amount + ' | bal: ' + cash + '] >> less cash!!');
            return callback({status: 'You have insufficient balance of KES ' + cash + ' to withdraw KES ' + with_amount});
        }

        //here
        let w_fee = (with_amount <= 1000) ? 16 : 23;
        let wht = configs.enforce_wht ? ((configs.wht_rate / 100) * with_amount) : 0;
        let amount_to_send = with_amount - (_.toNumber(w_fee) + _.toNumber(wht));
        with_amount = parseFloat(with_amount).toFixed(2);

        let unique_reference = Math.floor(new Date().getTime() / 10000);

        pg.query('INSERT INTO withdrawals(uid, amount, wallet, status, coin, charges, amount_sent, wht, sys_safe) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id', [id, with_amount, phone, 'Created', coin, w_fee, amount_to_send, wht, unique_reference], function (err, res) {
            if (err) {
                H.flog('b2c [' + phone + ' | ' + with_amount + ' | bal: ' + cash + ' | id: ' + unique_reference + '] | failed >> duplicate: ' + err.message, 'error');
                return callback({status: 'Duplicate request. Try again!'}, true);
            } else {
                Rule.deductBalance(id, with_amount, (new_balance, error) => {
                    if (error) {
                        return callback({status: 'There was an error withdrawing KES ' + with_amount + '. Try after some time.'});
                    }

                    let new_bal = _.toNumber(new_balance);
                    if (new_bal < 0) { //should not happen
                        H.flog('b2c [' + phone + ' | ' + with_amount + ' | bal: ' + cash + '] >> error! new bal less than 0 [' + new_bal + ']');
                        return callback({status: 'There was an error withdrawing KES ' + with_amount + '. Try after some time.'});
                    } else {
                        //m-pesa
                        let withdrawal_id = res.rows[0].id;
                        let client_reference = id + 'x' + withdrawal_id;

                        H.doB2C(phone, amount_to_send, client_reference, (err, request_id) => {
                            if (err) {
                                H.flog('b2c [' + phone + ' | ' + with_amount + ' [' + amount_to_send + '] | bal: ' + cash + ' | new bal: ' + new_bal + ' | id: ' + withdrawal_id + '] | failed >> ' + request_id, 'error');

                                pg.query("update withdrawals set status = $1 where id = $2", ['Pending', withdrawal_id], function (err, res) {
                                });

                                callback({status: 'Withdrawal of KES ' + with_amount + ' pending!'});
                            } else {
                                H.flog('b2c [' + phone + ' | ' + with_amount + ' [' + amount_to_send + '] | bal: ' + cash + ' | new bal: ' + new_bal + ' | id: ' + withdrawal_id + '] | success >> id: ' + request_id, 'notice');

                                pg.query("update withdrawals set status = $1, request_id = $2 where id = $3", ['Sent', request_id, withdrawal_id], function (err, res) {
                                });

                                callback({status: 'Withdrawal of KES ' + with_amount + ' scheduled successfully.'});
                            }
                        });
                    }
                });
            }
        }); //to here
    });
}

Rule.deductBalance = function (id, amount, callback) {
    if (!id && !amount) return;

    id = _.toNumber(id);

    pg.query("update users set balance = jsonb_set(balance::jsonb, '{kes}', (((balance->>'kes')::numeric - '" + amount + "'::numeric)::text)::jsonb, false) where id = $1 returning balance->>'kes' as new_balance", [id], function (err, res) {
        if (err) {
            callback(false, true);
        } else {
            let new_balance = res.rows[0].new_balance;
            callback(new_balance, false);
        }
    });
};

/*
 * Get Client Credit by Coin and Withdrawal Fee
 */
Rule.creditCoin = function (id, coin, callback) {
    if (!id && !coin) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);

    Rule.getClientCoinCredit(id, coin, (result) => {
        if (result === false) return;
        callback({
            credit: H.CryptoSet(result, coin),
            uid: id,
            fee: 16
        });
    });
}

/*
 * Get Client Wallet Address
 */
Rule.getWalletAddress = function (id, coin, callback) {
    if (!id && !coin) return;

    id = _.toNumber(id);
    coin = _.lowerCase(TOKEN_COIN);

    pg.query('SELECT address FROM wallets WHERE uid = $1 AND coin = $2', [id, coin], function (err, results) {
        if (err) {
            console.log('error on UserRule: 1310', err)
            return callback(false, true);
        }
        try {
            let row = results.rows[0];

            if (!_.isUndefined(row)) {
                callback({
                    uid: id,
                    wallet: coin,
                    address: row.address
                }, false);
            } else {
                cryptoMakeWallet(_.toNumber(id), _.lowerCase(coin), (result) => {
                    return callback({
                        uid: id,
                        wallet: coin,
                        address: result
                    });
                })
            }
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Update Client Wallet Address
 */
Rule.updateUserWallet = function (id, coin, address, callback) {
    if (!id && !coin && !address) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);

    if (_.isUndefined(address)) return callback(false, true)

    pg.query(`INSERT INTO wallets (uid, address, coin, network)
              VALUES ($1, $2, $3, $4)`, [id, address, coin, 'bsc'], function (err, ok) {
        if (!err) {
            callback(true, false);
        } else return callback(false, true)
    })
}

/*
 * Top Winners
 */
Rule.topWinners = function (callback) {
    pg.query(`SELECT name, profit_high, avatar, plays FROM users where status = 'Active' ORDER BY cast(profit_high ->> '${TOKEN_COIN}' as numeric) DESC LIMIT 10`, function (err, results) {
        if (err) {
            console.log('error on User Rule: 1094', err)
            return callback(false);
        }
        try {
            return callback(results.rows);
        } catch (e) {
            console.log(e)
        }
    })
}

/*
 * Get Top Referrers
 * used to get top referral earners
 */
Rule.topReferrers = function (limit, callback) {
    //select e.to_uid, u.name, u.user_id, sum(e.earnings)::numeric total_earnings from ref_earnings e left join users u on e.to_uid = u.id where (e.created between (date_trunc('week', current_date) - INTERVAL '60 hours') and (date_trunc('week', current_date) + INTERVAL '108 hours')) group by e.to_uid, u.name, u.user_id order by total_earnings DESC limit
    pg.query("select e.to_uid, u.name, u.user_id, sum(e.earnings)::numeric total_earnings from ref_earnings e left join users u on e.to_uid = u.id where (e.created >= '2022-02-11 12:00:00') group by e.to_uid, u.name, u.user_id order by total_earnings DESC limit $1", [limit], function (err, results) {
        if (err) {
            console.log('error on UserRule: 1952', err)
            return callback(false);
        }
        try {
            callback(results.rows);
        } catch (e) {
            console.log('Error on UserRule 1958: ' + e);
        }
    });
}

/*
 * Last Bets
 */
Rule.lastBets = function (callback) {
    pg.query('SELECT * FROM bets ORDER BY created DESC LIMIT $1', [13], function (err, results) {
        if (err) {
            console.log('error on User Rule lastBets', err)
            return callback(false, true);
        }
        try {
            callback(_.reverse(results.rows), false);
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Target Game BankRoll
 */
Rule.getBankRoll = function (game, coin, callback) {
    pg.query('SELECT * FROM bankroll WHERE game = $1', ['crash'], function (err, results) {
        if (err) {
            console.log('error on User Rule: 799', err)
            return callback(false, true);
        }

        //amount, weight, edge_percentage, edge_tolerance, formula_to_use
        return callback(results.rows[0], false);
    });
}

Rule.getTotalWagered = function (callback) { //~ where date(created) = CURRENT_DATE || ~ where created >= (NOW() - INTERVAL '1 DAY')
    pg.query("select sum(amount) as wagered from bets where user_type = 'u' and date(created) = CURRENT_DATE", [], function (err, results) {
        if (err) {
            console.log('error on User Rule: getting total wagered', err)
            return callback(false, true);
        }

        return callback(results.rows[0].wagered, false);
    });
}

Rule.getTotalWon = function (callback) { //~ where date(created) = CURRENT_DATE and profit > '0' || ~ where created >= (NOW() - INTERVAL '1 DAY') and profit > '0'
    pg.query("select sum(amount + profit) as won from bets where user_type = 'u' and date(created) = CURRENT_DATE and profit > '0'", [], function (err, results) {
        if (err) {
            console.log('error on User Rule: getting total won', err)
            return callback(false, true);
        }

        return callback(results.rows[0].won, false);
    });
}

Rule.getLastGameId = function (callback) {
    pg.query("select id from crashs order by id desc limit 1", [], function (err, results) {
        if (err) {
            console.log('error on User Rule: 1839', err)
            return callback(false, true);
        }

        return callback(results.rows[0].id, false);
    });
}

Rule.getNextGameHash = function (game_id, callback) {
    pg.query("select hash from sys_hashes where game_id = $1", [game_id], function (err, results) {
        if (err) {
            console.log('error on User Rule: 2030', err)
            return callback(false, true);
        }

        return callback(results.rows[0].hash, false);
    });
}

/*
 * Get Client Profit
 */
Rule.getUserProfit = function (id, coin, callback) {
    if (!id && !coin) return;

    id = _.toNumber(id)
    coin = _.lowerCase(coin)
    pg.query('SELECT profit FROM users WHERE id = $1', [id], function (err, results) {
        if (err) {
            console.log('error user role: 841', err);
            return callback(false, true);
        }
        ;
        callback(results.rows[0].profit[coin], false)
    });
}

/*
 * Get the user play count
 */
Rule.playingCount = function (id, callback) {
    pg.query("SELECT COUNT(*) from bets where uid = $1", [id], function (err, res) {
        if (err) {
            return callback(0);
        } else {
            let count = res.rows[0].count;
            return callback(count);
        }
    });
}

/*
 * Get the user win count
 */
Rule.winCount = function (id, callback) {
    pg.query("SELECT COUNT(*) from bets where uid = $1 AND profit != 0.00000000;", [id], function (err, res) {
        if (err) {
            return callback(0);
        } else {
            let count = res.rows[0].count;
            return callback(count);
        }
    });
}

/*
 * Get and Calculate User Medal
 */
Rule.getUserMedal = function (id, callback) {
    pg.query('SELECT SUM( amount ) FROM deposits WHERE uid = $1 AND coin = $2', [id, TOKEN_COIN], function (err, results) {
        if (err) return callback(0);
        else {
            if (results.rows.length === 0)
                return callback(0);

            let medal = 0;

            let deposited = _.toNumber(results.rows[0].sum);

            //100 USD
            let medal_A = 100.00000000;

            //500 USD
            let medal_B = 500.00000000;

            //2000 USD
            let medal_C = 2000.00000000;

            if (deposited >= medal_C)
                medal = 3;
            else if (deposited >= medal_B)
                medal = 2;
            else if (deposited >= medal_A)
                medal = 1;

            return callback(medal);
        }
    });
}

/*
 * Get the full User info
 */
Rule.userInfo = function (name, coin, first, callback) {
    if (!name && !coin) return;

    coin = _.lowerCase(coin);

    Rule.getUserInfoByName(name, function (details, err) {
        if (err || !details.id) {
            return callback({
                status: false
            });
        }

        let bestCoin = TOKEN_COIN;

        let netProfit = (details.profit[bestCoin] !== undefined) ? details.profit[bestCoin] : 0;
        let highProfit = (details.profit_high[bestCoin] !== undefined) ? details.profit_high[bestCoin] : 0;
        let lowProfit = (details.profit_low[bestCoin] !== undefined) ? details.profit_low[bestCoin] : 0;
        let avatar = details.avatar !== null ? details.avatar : UPLOAD_URL + 'avatar.png';
        let user_type = details.type !== null ? details.type : 'person';

        Rule.playingCount(details.id, (count) => {
            Rule.winCount(details.id, (win) => {
                Rule.getUserMedal(details.id, (medal) => {
                    callback({
                        status: true,
                        coin: _.upperCase(bestCoin),
                        played: (user_type === 'bot') ? (_.toNumber(count) + 1000) : count,
                        wined: (user_type === 'bot') ? (_.toNumber(win) + 1000) : win,
                        medal: medal,
                        name: name,
                        id: details.id,
                        avatar: avatar,
                        level: details.level,
                        created: details.created,
                        friend: details.friends,
                        profit: netProfit,
                        high_profit: highProfit,
                        low_profit: lowProfit
                    })
                })
            })
        })
    });
}


/*
 * Get Client Chart
 */
Rule.userChart = function (name, game, coin, callback) {
    if (!name && !game && !coin) return;

    coin = _.lowerCase(coin);

    pg.query('SELECT * FROM bets WHERE name = $1 AND coin = $2 AND game = $3 ORDER BY created DESC LIMIT 20', [name, coin, game], function (err, results) {
        if (err) {
            console.log('error on User Rule: 917', err)
            return callback(false, true);
        }
        if (results.rows.length === 0) {
            return callback([]);
        }
        try {
            callback({
                data: results.rows
            }, false);
        } catch (e) {
            console.log(e)
        }
    });
}

/*
 * Get Site notifications
 */
Rule.notifications = function (callback) {
    pg.query('SELECT * FROM notifications ORDER BY date DESC LIMIT 10', function (err, results) {
        callback(results.rows);
    });
}

/*
 * Get Game Details
 */
Rule.gameDetails = function (id, callback) {
    if (!id) return;

    id = _.toString(id)

    pg.query('SELECT * FROM bets WHERE gid = $1', [id], function (err, results) {
        if (err) {
            console.log('error on User Rule: 946', err)
            return callback(false, true);
        } else {
            pg.query('SELECT * FROM crashs WHERE gid = $1', [id], function (err, res) {
                if (err) {
                    console.log('error on User Rule: 946', err)
                    return callback(false, true);
                }

                let bets = [];

                if (!_.isUndefined(results.rows)) {
                    bets = results.rows;
                }

                let info = [];

                if (!_.isUndefined(res.rows[0])) {
                    info = res.rows[0];
                }

                callback({
                    id: id,
                    data: bets,
                    info: info
                })
            });
        }
    });
}

/*
 * Client Private Messages
 */
Rule.messages = function (token, id, name, callback) {
    if (!id && !name) return;

    Rule.getIdByName(name, function (target) {
        if (!target) {
            console.log('target not found USER ROLE: 1304');
            return false
        }
        let key = _.toNumber(id) + _.toNumber(target);

        pg.query('SELECT * FROM messages WHERE room_key = $1', [key], function (err, message) {
            if (err) return callback(err);

            let rows = [];

            if (!_.isUndefined(message.rows)) {
                rows = message.rows;
            }

            callback({
                message: rows
            })
        });
    })
}

/*
 * Send Private Messages
 *
 * Can be Optimize
 */
Rule.addMessages = function (token, id, to, message, callback) {
    if (!id && !message && !to) return;

    id = _.toNumber(id);
    let time = H.getCurrentTime(new Date());

    Rule.getIdByName(to, function (targetID) {
        if (!targetID) return;

        Rule.getNameById(id, function (sender) {
            if (!sender) return;

            let key = _.toNumber(id) + _.toNumber(targetID);

            pg.query('INSERT INTO messages (from_uid, to_uid, message, time, from_name, to_name, room_key) VALUES($1, $2, $3, $4, $5, $6, $7)', [id, targetID, message, time, sender, to, key], function (err, result) {
                if (err) return callback(err);

                callback({
                    uid: id,
                    target: targetID,
                    message: message,
                    time: time,
                    date: H.getCurrentDate(new Date()),
                    from_name: sender,
                    to_name: to
                })
            });
        });
    });
}

/*
 * Get The Last Game Bets
 */
Rule.lastGameBets = function (game, callback) {
    if (!game) return;

    pg.query('SELECT * FROM bets WHERE game = $1 ORDER BY created DESC LIMIT 10', [game], function (err, results) {
        if (err) {
            console.log('Error User Role: 1028', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = _.reverse(results.rows);
        }

        callback(rows);
    });
}

/*
 * Get The Game Bots
 */
Rule.getGameBots = function (game, callback) {
    if (!game && !time) return;

    pg.query('SELECT * FROM bots WHERE status = $2 AND game = $1 LIMIT 15', [game, 'active'], function (err, results) {
        if (err) {
            console.log('Error User Role: 1381', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        callback(rows);
    });
}

/*
 * Get The Random Bots by time
 */
Rule.getRandomBots = function (callback) {
    const time = new Date().getHours();

    pg.query('SELECT * FROM bots WHERE time = $1 AND status = $3 ORDER BY RANDOM() LIMIT $2', [time, BotsLimit, 'active'], function (err, results) {
        if (err) {
            console.log('Error User Role: 1753', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

Rule.getRandomBotsRegardlessOfTime = function (limit, callback) {
    const time = new Date().getHours();

    pg.query('SELECT * FROM bots WHERE status = $1 ORDER BY RANDOM() LIMIT $2', ['active', limit], function (err, results) {
        if (err) {
            console.log('Error User Role: 1753', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

Rule.getActiveBots = function (limit, callback) {
    pg.query('SELECT * FROM bots WHERE status = $1 LIMIT $2', ['active', limit], function (err, results) {
        if (err) {
            console.log('Error User Role: 1753', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

Rule.getAutoBets = function (callback) {
    pg.query('SELECT * FROM autobet where sys_status = $1', ['on'], function (err, results) {
        if (err) {
            console.log('Error User Role: 2461', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

/*
 * Get The Game Bots By Game and Time
 */
Rule.getBots = function (game, time, callback) {
    if (!game) return;

    const timer = new Date().getHours();

    pg.query('SELECT * FROM bots WHERE status = $3 AND game = $1 AND time = $2 LIMIT $4', ['crash', timer, 'active', BotsLimit], function (err, results) {
        if (err) {
            console.log('Error User Role: 1781', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

/*
 * Get The Bots for Chats by Time
 */
Rule.getBotsByTime = function (callback) {
    const timer = new Date().getHours();

    pg.query('SELECT * FROM bots WHERE status = $2 AND time = $1 ORDER BY RANDOM() LIMIT $3', [timer, 'active', 10], function (err, results) {
        if (err) {
            console.log('Error User Role: 1761', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

/*
 * Get The Game Bots for Chats
 */
Rule.getBotsForRapidChats = function (callback) {
    pg.query('SELECT * FROM bots WHERE status = $1 ORDER BY RANDOM() LIMIT $2', ['active', 1], function (err, results) {
        if (err) {
            console.log('Error User Role: 1913', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

/*
 * Get The Random Bots
 */
Rule.getTheRandomBots = function (callback) {
    pg.query('SELECT * FROM bots WHERE status = $1 ORDER BY RANDOM() LIMIT $2', ['active', BotsLimit], function (err, results) {
        if (err) {
            console.log('Error User Role: 1913', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows;
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

/*
 * Get The Bot from Users by name
 */
Rule.getBotByName = function (name, callback) {
    pg.query('SELECT * FROM users WHERE name = $1', [name], function (err, results) {
        if (err) {
            console.log('Error User Role: 1913', err);
            return callback(false);
        }

        let rows = [];

        if (!_.isUndefined(results.rows)) {
            rows = results.rows[0];
        }

        if (rows.length === 0)
            return callback(false);

        callback(rows);
    });
}

/*
 * Upload User Avatar
 */
Rule.uploadAvatar = function (client, event, uploader, callback) {
    if (!client && !event) return;

    let type = H.baseName(event.file.name).trim();

    if (type !== 'jpg' || type !== 'png') {
        // uploader.abort(event.file.id, client);
    }

    let token = event.file.meta.token;
    if (!token) return;

    let avatar = UPLOAD_URL + event.file.name;

    if (_.isObject(token)) { // For Admin
        Rule.getIdByName(token.name, function (uid) {
            if (!uid) return;

            pg.query('UPDATE users SET avatar = $1 WHERE id = $2', [avatar, uid], function (err, res) {
                if (err) {
                    console.log('error user role: 1258', err)
                    return callback(false);
                }
                return callback({
                    status: true,
                    file: event.file.name
                });
            });
        });
    } else {
        //Save Avatar For User
        Token.getID(token, (uid) => {
            if (uid === null) return;
            pg.query('UPDATE users SET avatar = $1 WHERE id = $2', [avatar, uid], function (err, result) {
                if (err) {
                    console.log('error user role: 1271', err)
                    return callback(false);
                }
                callback({
                    status: true,
                    file: event.file.name
                });
            });
        });
    }
}

/*
 * Bulk Change Database
 */
Rule.bulkChange = function (username, id, callback) {
    if (!username && !id) return;

    let uid = _.toNumber(id);
    let rooms = ['chat_global', 'chat_spam'];

    //Change on Chat rooms
    rooms.forEach((name, i) => {
        pg.query('UPDATE ' + name + ' SET name = $1 WHERE uid = $2', [username, uid], function (err, res) {
            if (err) {
                console.warn(err);
                return callback(err);
            }
        });
    });

    //Change on Bets
    pg.query('UPDATE bets SET name = $1 WHERE uid = $2', [username, uid], function (err, res) {
        if (err) {
            console.warn(err);
            return callback(err);
        }
    });

    //Change on Private Messages
    pg.query('UPDATE messages SET from_name = $1 WHERE from_uid = $2', [username, uid], function (err, res) {
        if (err) {
            console.warn(err);
            return callback(err);
        }
    });
    pg.query('UPDATE messages SET to_name = $1 WHERE to_uid = $2', [username, uid], function (err, res) {
        if (err) {
            console.warn(err);
            return callback(err);
        }
    });

    callback(true);
}

/*
 * Make Rain
 */
Rule.makeRain = function (token, id, amount, coin, players, room, callback) {
    if (!coin && !amount && !id && !players && !room) return;

    id = _.toNumber(id);
    coin = _.lowerCase(coin);
    amount = _.toNumber(amount);

    let table = 'chat_' + _.lowerCase(room);
    let locked = false;
    let list = [];
    let ids = [];

    //Check if User Have Credit
    Rule.getUserInfo(id, (result, er) => {
        if (er) return;

        let balance = result.balance;
        let bc = balance[coin];

        if (_.isUndefined(bc))
            bc = '0.00000000';

        let credit = _.toNumber(bc);
        let allAmount = (amount * _.toNumber(players));
        allAmount = _.toNumber(allAmount);

        if (_.isUndefined(credit)) {
            console.log('ERROR USER CREDIT ! 1', credit)
            return callback(false, {
                status: false,
                message: 'Your Credit is Not enough!'
            }, true);
        }

        if (credit === 0) {
            console.log('ERROR USER CREDIT ! 2', credit)
            return callback(false, {
                status: false,
                message: 'Your Credit is Not enough!'
            }, true);
        }
        ;

        if (credit < allAmount) {
            console.log('ERROR USER CREDIT ! 3', credit)
            return callback(false, {
                status: false,
                message: 'Your Credit is Not enough!'
            }, true);
        }
        ;

        pg.query(`SELECT uid, name
                  FROM ${table}
                  ORDER BY sorter DESC
                  LIMIT $1`, [200], function (err, results) {
            if (err) {
                console.log('Error RAIN USER Role: 2210', err);
                return callback(false, {
                    status: false,
                    message: 'Please try again later'
                }, true);
            }

            results.rows.forEach((user, i) => {
                Rule.getNameById(id, (senderName) => {
                    if (user.name !== 'SystemBot') {
                        if (user.name !== senderName) {
                            if (_.toNumber(user.uid) !== id) {
                                let r = '@' + user.name;
                                if (list.indexOf(r) === -1) {
                                    if (list.length < _.toNumber(players)) {
                                        list.push(r);
                                        ids.push(user.uid)
                                    }
                                }
                            }
                        }
                    }
                    ;
                })
            }) // End Foreach

            let SENDED = false;

            //Added
            H.wait(1000).then(() => {
                if (list.length === _.toNumber(players)) {
                    //Reduce Client Amount
                    Rule.reduceBalance(id, allAmount, coin, 'rain', (result, er) => {
                        if (er) {
                            H.log('info', `ERROR on reduceBalance: 2244`);
                            return callback(false, {
                                status: false,
                                message: 'Please try again later'
                            }, true);
                        } else {
                            //Add Balance to Users
                            for (let i = 0; i < ids.length; i++) {
                                Rule.addBalance(_.toNumber(ids[i]), amount, 0, coin, (rr, newBonus, errr) => {

                                    //Emit On Chat
                                    Rule.getNameById(id, (senderName) => {
                                        if (!senderName) return;

                                        let RoboID = 7153444;
                                        let RoboName = 'SystemBot';
                                        let avatar = UPLOAD_URL + '1241124121414121412.png';
                                        let sorter = _.toNumber(new Date().getTime());
                                        let time = H.getCurrentTime(new Date());

                                        let message = `@${senderName} Has Been Rained ${H.CryptoSet(amount * _.toNumber(players), coin)} ${_.upperCase(coin)} on: ${list}`


                                        //Add Message then Boardcast to Clients
                                        pg.query('INSERT INTO ' + table + '(name, uid, avatar, message, time, sorter) VALUES($1, $2, $3, $4, $5, $6)', [RoboName, RoboID, avatar, message, time, sorter],
                                            function (err, result) {
                                                if (err) {
                                                    H.log('info', `error on insert to table in rain`);
                                                    return callback(false, {
                                                        status: false,
                                                        message: 'Please try again later'
                                                    }, true);
                                                } else {
                                                    if (!SENDED) {
                                                        SENDED = true;
                                                        return callback({
                                                            country: _.lowerCase(room),
                                                            message: message,
                                                            name: RoboName,
                                                            avatar: avatar,
                                                            level: 1,
                                                            time: time,
                                                            date: H.getCurrentDate(new Date()),
                                                            sorter: sorter
                                                        }, {
                                                            message: 'Rained Done !',
                                                            token: token
                                                        }, false)
                                                    }
                                                }
                                            });
                                    })
                                })
                            }
                        }
                    });
                }
            })
        });
    });
}

/*
 * Generate Wallet Using cryptoapi.io
 */
function cryptoMakeWallet(uid, coin, callback) {
    if (!coin && !uid) return;

    uid = _.toNumber(uid);
    coin = _.lowerCase(coin);

    // BitcoinClient.getNewAddress(_.toString(uid), function(err, wallet) {
    //     if (err) {
    //         return callback('Please Try Again Later. -11' + err)
    //     }

    //     Rule.updateUserWallet(uid, coin, wallet, function(result) {
    //         if (result) {
    //             return callback(wallet);
    //         } else {
    //             return callback('Please Try Again Later. -22')
    //         }
    //     });
    // });
}

Rule.getJackpot = function (jackpot_id, callback) {
    if (!jackpot_id) return;
    jackpot_id = _.toNumber(jackpot_id);

    pg.query('SELECT * FROM jackpot WHERE id = $1', [jackpot_id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 2879')
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0])) {
                if (results.rows[0].length === 0)
                    return callback(false, true)
                else
                    callback(results.rows[0], false);
            } else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
};

Rule.getActiveJackpots = function (callback) {
    pg.query("SELECT * FROM jackpot WHERE completion < $1 order by id desc limit 1", [100], function (err, results) {
        if (err) {
            console.log('error on UserRule: 2856', err)
            return callback(false, true);
        }
        try {
            if (!_.isUndefined(results.rows[0])) {
                if (results.rows[0].length === 0)
                    return callback(false, true)
                else
                    callback(results.rows[0], false);
            } else
                callback(false, true)
        } catch (e) {
            console.log(e)
        }
    });
}

Rule.getJackpotTopThree = function (jackpot_id, callback) {
    if (!jackpot_id) return callback(false, true);

    jackpot_id = _.toNumber(jackpot_id);

    pg.query("SELECT u.name, u.full_name, b.* FROM jackpot_bets b join users u on b.uid = u.id WHERE b.jackpot_id = $1 order by (b.cashout*1) desc limit 3", [jackpot_id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 2850', err)
            return callback(false, true);
        }
        try {
            callback((results.rows), false);
        } catch (e) {
            console.log(e)
        }
    });
}

Rule.getJackpotMine = function (uid, jackpot_id, callback) {
    if (!uid && !jackpot_id) return;
    uid = _.toNumber(uid);
    jackpot_id = _.toNumber(jackpot_id);

    pg.query("SELECT * FROM jackpot_bets WHERE uid = $1 and jackpot_id = $2 order by (cashout*1) DESC LIMIT 3", [uid, jackpot_id], function (err, results) {
        if (err) {
            console.log('error on UserRule: 2922', err)
            return callback(false, true);
        }
        try {
            callback((results.rows), false); //_.reverse
        } catch (e) {
            console.log(e)
        }
    });
}

//Export Users Rule Module
module.exports = Rule;