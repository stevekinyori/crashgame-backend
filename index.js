const express = require('express');
const server = express();
const app = require('http').createServer(server);
const config = require('./config');
const io = require('socket.io')(app, {
    cors: {
        origin: config.cors_origins,
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    }
});
const bodyParser = require('body-parser');
const compr = require('compression');
const siofu = require("socketio-file-upload");
const cors = require("cors");
const xss = require('xss-clean');
const helmet = require('helmet');
const _ = require('lodash');

const Admin = require('./Admin');
const Users = require('./Users');
const Games = require('./Games');
const Crash = require('./Games/Crash');
const Deposit = require('./Wallet/Deposit');

const connections = new Set();

// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({extended: false}))

// parse application/json
server.use(bodyParser.json())

//Server Security
server.use(xss());
server.use(cors());
server.use(helmet());
server.use(compr());

//Upload Image
server.use(siofu.router);

server.get('/', function (req, res, next) {
    res.sendStatus(403);
});

// Load Files (Avatar)
server.get('/uploads/*', function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    let file = req.params[0];
    res.sendFile(__dirname + '/uploads/' + file);
});

//Get Current Hour
server.get('/hour', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    let hour = new Date().getHours();
    res.json({hour});
});

//Update Users Deposit
server.post('/deposit', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

    //deactivated
    res.send('not allowed!!');

    /*let ip_h = req.headers['x-forwarded-for'];
    let ip = req.ip;
    let api_key = req.headers['h_api_key'];

    //console.log("[Dan] deposit: ip: " + ip_h + " [" + ip + "] | data:", req.body);

    let ips = ['::1', 'localhost', '127.0.0.1', '172.16.1.13', '::ffff:172.16.1.13'];
    if (ips.includes(ip) && api_key === '235142a18b1962fcb3b26ca50d37b87f20fe34935c4e53da7737677f4f59de4e') {
        Deposit.update(io, req.body);
        res.send('Success');
    } else {
        res.send('Failed;Invalid authentication [Err P or K]');
    }*/
});

server.post('/notify', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

    let ip_h = req.headers['x-forwarded-for'];
    let ip = req.ip;
    let api_key = req.headers['h_api_key'];

    //console.log("[Dan] notify: ip: " + ip_h + " [" + ip + "] | data:", req.body);

    let ips = ['::1', 'localhost', '127.0.0.1', '172.16.1.13', '::ffff:172.16.1.13'];
    if (ips.includes(ip) && api_key === '235142a18b1962fcb3b26ca50d37b87f20fe34935c4e53da7737677f4f59de4e') {
        Deposit.notify(io, req.body);
        res.send('Success');
    } else {
        res.send('Failed;Invalid authentication [Err P or K]');
    }
});

/**
 * Socket Event Handlers
 */
let setEventHandlers = function () {
    console.log('API Server running on port:', config.port);
    /**
     * Crash Idle
     */
    Crash.Idle(io);

    /**
     * Listen To Public Socket Requests
     */
    io.sockets.on('connection', onPublicConnection);

    /**
     * Listen To Private Socket Requests
     */
    const admin = io.of('/admin_');

    admin.use((socket, next) => {
        next();
    });

    admin.on('connection', onPrivateConnection);
}

/**
 * Public Socket Connection Response
 */
function onPublicConnection(client) {
    connections.add(client.id);
    //console.log('Client Connected: ' + client.id + " | Online: " + connections.size);
    io.emit('online_users', connections.size + 11);

    /*
     * Users Events
     */
    Users(client, io, siofu);

    /*
     * Games Events
     */
    Games(client, io);

    client.on('disconnect', function () {
        connections.delete(client.id);
        //console.log('Client Disconnected: ' + client.id + " | Online: " + connections.size);
        io.emit('online_users', connections.size + 11);
    });

    client.on('give_me_online_users', function () {
        //console.log('[Temp] give_me_online_users [' + client.id + ']: online now: ' + connections.size);
        io.emit('online_users', connections.size + 11);
    });
}

/**
 * Private Socket Connection Response
 */
function onPrivateConnection(client) {
    console.log('Admin Connected:', client.id);
    /*
     * Admin Events
     */
    Admin(client, io);
}

/**
 * Start listening for events
 */
setEventHandlers();

/*
 * Procces on Exit
*/
if (!config.developer) {
    process.stdin.resume();

    function exitHandler(options, exitCode) {
    }

    process.on('uncaughtException', exitHandler.bind(null, {exit: true}));
}

/**
 * Start App
 */
app.listen(config.port);
