// This is games queue, all players can be first add to this queue

const Socket = {

    // A storage object to hold the sockets
    sockets: {},

    // Adds a socket to the storage object so it can be located by uid
    add: function(socket, uid) {
        uid = parseInt(uid);
        this.sockets[uid] = socket;
    },

    // Returns a socket from the storage object based on its uid
    // Throws an exception if the uid is not valid
    get: function(uid) {
        uid = parseInt(uid);
        if (this.sockets[uid] !== undefined || this.sockets[uid] !== 'undefined'){
            return this.sockets[uid];
        } else {
            return console.log("A socket with the uid '"+ uid +"' does not exist");
        }
    },

    // Removes a socket from the storage object based on its uid
    remove: function(uid) {
        if (this.sockets[uid] !== undefined) {
            this.sockets[uid] = null;
            delete this.sockets[uid];
        }
    }
};

module.exports = Socket;