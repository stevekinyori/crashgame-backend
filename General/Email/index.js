var assert = require('assert');
var nodemailer = require('nodemailer');
var sesTransport = require('nodemailer-ses-transport');
var config = require('../../config');

var apiURL = config.apiURL;

function send(details, callback) {
    assert(details, callback);

    let transport = nodemailer.createTransport({
        service: 'gmail',
        port:465,
        secure: true, // true for 465, false for other ports
        logger: true,
        debug: true,
        secureConnection: false,
        auth: {
            user: 'satcrashio@gmail.com',
            pass: 'SatCrash@138'
        },
    });
    
    transport.sendMail(details, function (err) {
        if (err) {
            return callback(err);
        }
        callback(true);
    });
}

exports.passwordReset = function (to, password, callback) {
    var html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
        '<html xmlns="http://www.w3.org/1999/xhtml">' +
        '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
        '<title>MoneyPot</title>' +
        '</head>' +
        '<body>' +
        '<h2>Your Password</h2>' +
        '<br>' +
        password +
        '<br>' +
        '<br>' +
        "<span>We only send password resets to this email address." +
        '</body></html>';

    var details = {
        to: to,
        from: 'noreply@' + apiURL,
        subject: 'Reset Password Request',
        html: html

    };
    send(details, callback);
};