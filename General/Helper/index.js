// Helper can be used in other functions, that these functions are responsible for all data, they have a direct impact on performance.

const _ = require('lodash');
const formatCurrency = require('format-currency');
const pg = require("../Model");
const fs = require('fs');
const axios = require('axios');
const clc = require('cli-color');
const crypto = require("crypto");
const unirest = require("unirest");
const configs = require("../../config");

const algorithm = "aes-256-cbc";
const initVector = '62ea9dbd8903da13'; //16 characters
const SecurityKey = 'd2a3fdd51c369c869f3d68d245a7aaeb'; //32 characters

const Helper = {};

/*
 * Encryption methods
 */
Helper.encrypt = function (text) {
    const cipher = crypto.createCipheriv(algorithm, SecurityKey, initVector);
    let encryptedData = cipher.update(text, "utf-8", "hex");
    encryptedData += cipher.final("hex");

    return encryptedData;
}

Helper.decrypt = function (encryptedData) {
    const decipher = crypto.createDecipheriv(algorithm, SecurityKey, initVector);
    let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
    decryptedData += decipher.final("utf8");

    return decryptedData;
}

/*
 * Get Settings
*/
Helper.setting = function (key, callback) {
    pg.query('SELECT * FROM settings WHERE ID = 1', function (err, result) {
        if (err) return console.log('error on getting settings: 15', err);
        if (!_.isUndefined(result.rows)) {
            callback(result.rows[0][key]);
        }
    })
}

/*
 * Load a file
*/
Helper.load = function (path, callback) {
    let file = fs.readFileSync(path);
    file = JSON.parse(file);
    return callback(file);
}

/*
 * Save a file
*/
Helper.save = function (file, data) {
    return fs.writeFileSync(file, JSON.stringify(data, null, 4), {
        flags: 'w',
        encoding: 'utf-8',
        mode: 0o666,
        authClose: true
    });
}

/*
 * Get the Max int from Array
*/
Helper.arrayMax = function (arr) {
    return arr.reduce(function (p, v) {
        return (p > v ? p : v);
    });
}

/*
 * Write Log
 * @param type : info | debug | error
*/
Helper.log = function (type, data) {
    data = JSON.stringify(data);
    pg.query('INSERT INTO logs(info) VALUES($1)', [data], function (err, res) {
        if (err) {
            console.log('error on writing log.', err);
            console.log(data);
        }
    })
}

Helper.flog = function (log, level = 'info') {
    switch (level) {
        case 'error':
            console.log(clc.red(Helper.getLogTime() + " | " + log));
            break;
        case 'warning':
            console.log(clc.yellow(Helper.getLogTime() + " | " + log));
            break;
        case 'notice':
            console.log(clc.blue(Helper.getLogTime() + " | " + log));
            break;
        default:
            console.log(Helper.getLogTime() + " | " + log);
            break;
    }
}

Helper.getLogTime = function () {
    //dates
    let now_local = new Date();
    now_local.setMinutes(now_local.getMinutes() - now_local.getTimezoneOffset());
    return now_local.toISOString().replace('T', ' ').replace('Z', '');
}

/*
 * Fix 1.5e-7 format number to crypto format
*/
Helper.cryptoFormat = function (x) {
    if (Math.abs(x) < 1.0) {
        var e = parseInt(x.toString().split('e-')[1]);
        if (e) {
            x *= Math.pow(10, e - 1);
            x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
        }
    } else {
        var e = parseInt(x.toString().split('+')[1]);
        if (e > 20) {
            e -= 20;
            x /= Math.pow(10, e);
            x += (new Array(e + 1)).join('0');
        }
    }
    return parseFloat(x).toFixed(1);
}

/*
 * Set Amount to Crypto Format
*/
Helper.CryptoSet = function (val, coin) {
    return parseFloat(val).toFixed(2);
}

/*
 * Make Random String
 */
Helper.randomString = function (length) {
    var chars = '0123456789abcdefghiklmnopqrstuvwxyz'.split('');
    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}

/*
 * Make Random Integer
 */
Helper.randomIntger = function (length) {
    var chars = '123456789'.split('');
    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}

/*
 * Get Random Int
 */
Helper.getRandomInt = function (length) {
    return Math.floor(Math.random() * length);
}

/*
 * Get Random Integer Between
*/
Helper.getRandomBetween = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
 * Up First Word
*/
Helper.capFirst = function (string) {
    if (string === undefined) return;
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*
 * Get Current Time
 */
Helper.getCurrentTime = function (now) {
    var hr = now.getHours();
    var min = now.getMinutes();
    hr = (hr < 10) ? '0' + hr : '' + hr;
    min = (min < 10) ? '0' + min : '' + min;
    return hr + ':' + min;
}

/*
 * Get Current Date
 */
Helper.getCurrentDate = function (now) {
    var y = now.getUTCFullYear();
    var m = (now.getUTCMonth() + 1);
    var d = now.getUTCDate();
    return y + "-" + m + "-" + d
}

/*
 * Validate Number
*/
Helper.isValidNumber = function (val) {
    if (_.isUndefined(val)) return false;

    if (_.toNumber(val) <= 0) return false;
    if (_.toNumber(val) === 0) return false;
    if (_.toNumber(val) === 0.00000000) return false;

    if (!isNaN(val)) {
        var check = /^[-]?\d+|\d+.$/.test(val);
        if (check) {
            return true;
        }
    }
    return false;
}

/*
 * Make Unique Game ID by Date
*/
Helper.makeGameID = function () {
    let current = new Date();
    //let date = current.getYear().toString() + current.getUTCMonth().toString() + current.getUTCDay().toString() + current.getUTCHours().toString() + current.getUTCMinutes().toString() + current.getUTCSeconds().toString();
    //return date.substr(5).toString() + Math.floor(Math.random() * 100);
    return Math.floor(current.getTime() / 1000);
}

/*
 * Get Only Name from String
*/
Helper.baseName = function (str) {
    return _.toString(_.reverse(_.split(str, '.'))[0]);
}

Helper.sendSmsMultiple = function (phones, message, sender_name, api_key) {
    phones.forEach((phone, i) => {
        Helper.sendSms(phone, message, sender_name, api_key);
    });
}

Helper.sendSms = function (phone, message, sender_name, api_key) {

    //console.log("[" + phone + "] send sms - sender: " + sender_name + " | message: " + message);

    let data = JSON.stringify({
        "sender_name": sender_name,
        "mobile": phone,
        "message": message,
        "service_id": "0",
        "link_id": ""
    });

    let config = {
        method: 'post',
        url: 'https://pay.vashub.co.ke/sms/sendsms',
        headers: {
            'h_api_key': api_key,
            'Content-Type': 'application/json'
        },
        data: data
    };

    axios(config)
        .then(function (response) {
            //console.log("[" + phone + "] send sms response >> " + JSON.stringify(response.data));
        })
        .catch(function (error) {
            console.log("[" + phone + "] send sms error: " + error);
        });
}

Helper.toComma = function (value) {
    return formatCurrency(value);
}

Helper.doB2C = function (phone, amount, client_ref, callback) {
    let req = unirest('POST', 'https://dev.vashub.co.ke/api/b2c')
        .headers({
            'h_api_key': configs.PAYMENT_API_KEY,
            'Content-Type': 'application/json'
        })
        .send(JSON.stringify({
            "msisdn": phone,
            "amount": amount,
            "paybill": configs.PAYBILL_B2C,
            "client_ref": client_ref,
            "payment_type": "PromotionPayment",
        }))
        .timeout(20000)
        .end(function (res) {
            if (res.error) {
                Helper.flog("b2c [" + phone + " | " + amount + "] failed >> " + res.error, 'error');
                return callback(true, res.error);
            } else {
                //{"request_id":"0","request_status":"13","request_desc":"duplicate client request - Payment already settled","request_amount":"10","request_fee":"0.00","balance":"0.00","client_ref":8787767565}
                let {request_id, request_status, request_desc} = JSON.parse(res.raw_body);
                Helper.flog("b2c [" + phone + " | " + amount + "] sent >> " + res.raw_body + " | id: " + request_id + " | st: " + request_status + " | desc: " + request_desc, 'notice');
                if (request_status === '1') {
                    return callback(false, request_id);
                } else {
                    return callback(true, request_desc);
                }
            }
        });
}

/*
 * Set TimeOut with Promise
*/
Helper.wait = (ms) => new Promise(resolve => setTimeout(resolve, ms))

module.exports = Helper;