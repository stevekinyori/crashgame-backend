const { Client } = require('pg');
const config = require('../../config');

const client = new Client({
	connectionString: config.dbUrl
});

client.connect();

exports = module.exports = function () {
	if (arguments.length && arguments[0] instanceof http.Server) {
		return attach.apply(this, arguments);
	}
	return exports.Server.apply(null, arguments);
};

// Export the Postgres Client module
module.exports = client;