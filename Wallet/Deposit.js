let _ = require('lodash');
let md5 = require('md5');
let UserRule = require('../Users/Rule');
let pg = require('../General/Model');
let H = require('../General/Helper');
let C = require('../General/Constant');
let config = require('../config');
const {encode} = require('../General/Buffer');

let REFERRAL_PERCENTAGE = _.toNumber(config.REFERRAL_PERCENTAGE);

// Declare Payment object
const Payment = {};

/*
 * CryptoApi Deposit Update
 */
function startChecker(io, requests) {
    //console.log(H.getLogTime() + ' | deposit >> startChecker hit: ', requests);
    if (!requests) {
        return
    }

    let {phone, txid, amount, bonus, full_name} = requests;
    console.log(H.getLogTime() + ' | deposit [' + phone + ' | ' + amount + ' [' + bonus + '] | ' + txid + '] >> phone:', phone, ' | txid:', txid, ' | amt: ', amount);

    if (_.isNaN(amount)) {
        console.log(H.getLogTime() + ' | deposit [' + phone + ' | ' + amount + ' [' + bonus + '] | ' + txid + '] >> returned by nan');
        return;
    }

    UserRule.getUserByPhone(_.toNumber(phone), (user) => {

        if (!user) {
            console.log(H.getLogTime() + ' | deposit [' + phone + ' | ' + amount + ' [' + bonus + '] | ' + txid + '] >> no user with phone:', phone);
            return;
        }

        let uid = user.id;
        let user_full_name = user.full_name;
        let referrer_id = user.referrer_id;

        console.log(H.getLogTime() + ' | deposit [' + phone + ' | ' + amount + ' [' + bonus + '] | ' + txid + '] >> found user id: ' + uid + ' [' + user_full_name + '] ref: ' + referrer_id);

        if (user_full_name == null) {
            UserRule.updateUserNames(uid, full_name);
        }

        let txid_in = txid;
        let confirmations = 'Done';
        amount = parseFloat(amount);
        let coin = 'kes';

        pg.query('SELECT * FROM deposits WHERE txtid = $1', [txid_in], function (err, results) {
            // if not exists
            if (results.rows.length === 0) {
                // Save txid to db
                const query = "INSERT INTO deposits(uid, amount, salt, txtid, status, coin) VALUES($1, $2, $3, $4, $5, $6)";

                pg.query(query, [uid, amount, md5(txid_in), txid_in, confirmations, coin], function (err, re) {
                    //console.log(H.getLogTime() + ' | deposit >> after insert');

                    if (err) {
                        console.log(H.getLogTime() + ' Deposit : 45', err);
                        return false;
                    }

                    let deposit_message = bonus > '0' ? "Account credited with KES " + amount + " & Bonus of KES " + bonus : "Account credited with KES " + amount;

                    //Add User Credit
                    UserRule.addBalance(uid, amount, 0, coin, (result, newBonus, err) => {
                        io.emit(C.UPDATE_PAYMENT_STATUS, encode({
                            uid: uid,
                            message: deposit_message,
                            amount: amount,
                            coin: coin
                        }));
                    })

                    //referrer
                    if (referrer_id !== '0') {
                        UserRule.getUserByUserId(referrer_id, (result, er) => {
                            if (er) {
                                H.flog("[temp] referrer " + referrer_id + " not found!!");
                            } else {
                                let r_uid = result.id;
                                let earnings = REFERRAL_PERCENTAGE * amount;
                                H.flog("[" + uid + " | " + phone + "] found referrer: " + referrer_id + " >> " + r_uid + " | earn: " + earnings);
                                //todo -- save earnings & update referrals table for this user
                                pg.query('UPDATE users SET ref_earnings = round(ref_earnings + $1, 2) WHERE id = $2', [earnings, r_uid], function (err, res) {
                                    if (err) {
                                        console.log(H.getLogTime() + " | error updating referrer earnings: user_id " + referrer_id + " | uid: " + r_uid + " | earnings " + earnings + " | err:", err);
                                    }
                                });
                                //save earnings record
                                pg.query("INSERT INTO ref_earnings (from_uid, to_uid, amount, earnings, referral_rate) VALUES($1, $2, $3, $4, $5)", [uid, r_uid, amount, earnings, (REFERRAL_PERCENTAGE * 100)], function (err, re) {
                                    if (err) {
                                        console.log(H.getLogTime() + 'error saving referrer earnings, 98', err);
                                    }
                                });
                            }
                        });
                    }

                })
            } else {
                console.log(H.getLogTime() + ' | deposit [' + phone + ' | ' + amount + ' [' + bonus + '] | ' + txid + '] >> trx ', txid_in, ' exists!!');
                io.emit(C.UPDATE_PAYMENT_STATUS, encode({
                    uid: uid,
                    message: "Deposit with Code " + txid_in + " exists!",
                    amount: 0,
                    coin: 'kes'
                }))
            }
        })
    })
}

Payment.update = function (io, requests) {
    return startChecker(io, requests)
};

Payment.notify = function (io, requests) {
    //console.log(H.getLogTime() + ' |notify hit: ', requests);
    if (!requests) {
        return;
    }

    let {phone, message} = requests;

    UserRule.getUserByPhone(_.toNumber(phone), (user) => {

        if (!user) {
            console.log(H.getLogTime() + ' | notify [' + phone + '] >> no user with phone:', phone);
            return;
        }

        let uid = user.id;
        let user_full_name = user.full_name;
        let name = user.name;

        //console.log(H.getLogTime() + ' | notify [' + phone + '] >> found user id: ' + uid + ' | name: ' + name + ' [' + user_full_name + ']');

        io.emit(C.CONFIRM_PAYMENT_STATUS, encode({
            uid: uid,
            message: message,
            amount: 0,
            coin: 'kes'
        }));
    })
};

// Export the Payment module
module.exports = Payment;