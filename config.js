let os = require("os");
let hostname = os.hostname();

const developer = (hostname === 'iKinyori');

let port = 80;
let postgresDB = 'postgres://bust:bust123.!@localhost:5432/bust';
if(developer) {
    port = 3004;
    postgresDB = 'postgres://bust:bust123.!@localhost:5432/bust';
}

let apiURL = 'https://bustapi.jazabet.com';
let secretRecaptcha = '6LcrkfwcAAAAAEF4T7_0MuVnbSxb5pAFo1gjZXQ4';
let avatarUrl = apiURL + '/uploads/'
let house = 10;

const api_key = '58ed1304e5c76e71ef34fea15a3e262375d0ca582f1e864d1aa4471ea4aea817';
const sender_name = 'JazaBET';

const PAYMENT_API_KEY = 'dbda5028a3097a80b6ca052d91f6204154390407a8e3';
const PAYBILL = '4048501'; //'779779';
const PAYBILL_B2C = '4069373';
const PAYBILL_ACCOUNT = 'PLAYA';
const STK_CALLBACK = 'http://localhost/hc_apps/jp/jp_stk_dlr.php';
const MINIMUM_WITHDRAW = 10;

const domain = "https://jazabet.com";
const cors_origins = ["https://jazabet.com", "https://www.jazabet.com", "http://192.168.1.7:3000", "http://dan:3000", "https://jazabet-crash.web.app", "http://xxjazabet.local:3000", "https://multiplaya-8d85c.web.app", "https://multiplaya-c0b25.web.app"];

// Email API
const emailApi = "https://email.jazabet.com/reset";

// Bots Limit Playing
const BotsLimit = 15;

const excise_rate = 7.5;
const wht_rate = 20;
const enforce_excise = true;
const enforce_wht = false;

// Default referral percentage
const referral_percentage = "0.02"; //% e.g 2/100

// Default Coin / Token
const coin = 'kes';

// Default Balance
const BALANCE = {
    "kes": '0.00', "bonus": '0.00', "demo": '0.00'
};

// Default Wallet
const WALLET = {
    "kes": '0.00', "bonus": '0.00', "demo": '0.00'
};

module.exports = {
    developer: developer,
    dbUrl: postgresDB,
    house: house,
    port: port,
    API_URL: apiURL,
    secretRecaptcha: secretRecaptcha,
    REFERRAL_PERCENTAGE: referral_percentage,
    avatarUrl: avatarUrl,
    BotsLimit: BotsLimit,
    EMAIL_API: emailApi,
    BALANCE: BALANCE,
    WALLET: WALLET,
    COIN: coin,
    sender_name: sender_name,
    api_key: api_key,
    excise_rate: excise_rate,
    wht_rate: wht_rate,
    enforce_excise: enforce_excise,
    enforce_wht: enforce_wht,
    PAYBILL: PAYBILL,
    PAYBILL_B2C: PAYBILL_B2C,
    PAYBILL_ACCOUNT: PAYBILL_ACCOUNT,
    PAYMENT_API_KEY: PAYMENT_API_KEY,
    STK_CALLBACK: STK_CALLBACK,
    MINIMUM_WITHDRAW: MINIMUM_WITHDRAW,
    domain: domain,
    cors_origins: cors_origins,
};