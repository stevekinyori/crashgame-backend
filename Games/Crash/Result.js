'use strict';

//Required Libs
let SHA256 = require("crypto-js/sha256");
const crypto = require("crypto");
const HmacSHA256 = require("crypto-js/hmac-sha256");
const HEX = require("crypto-js/enc-hex");
const Helper = require("../../General/Helper"); //for paka

// Declare Random object
const Random = {};

let max_crash = Helper.getRandomBetween(300, 600).toFixed(2);

Random.generateResult = function (encrypted_hash, this_game_id, weight = 99) {
    //let gameHash = H.randomString(64);
    let gameHash = Helper.decrypt(encrypted_hash);

    let crashPedro = crashPointFromSeedPedro(gameHash, weight);
    let crashBust = crashPointFromSeed(gameHash, weight);
    let crashPaka = crashPointFromHashPaka(gameHash, weight);
    let crashBc = crashPointFromHashBC(gameHash, weight);

    return {hash: gameHash, crash_pedro: crashPedro, crash_bust: crashBust, crash_paka: crashPaka, crash_bc: crashBc};
}

//Pedro
function crashPointFromSeedPedro(seed, weight) {
    let hash = SHA256(seed).toString();

    let h = parseInt(hash.slice(0, 13), 16);
    let e = Math.pow(2, 52);
    //let result = Math.floor((98 * e) / (e - h));
    let result = Math.floor((weight * e - h) / (e - h)); //Dan - bustabit
    let max = (result / 100).toFixed(2);

    max = Math.max(1.00, max); //Dan - 1.0 -- min crash 1.10

    //max
    max = Math.min(max_crash, max); //Dan -- crash point cannot be more than 100

    return max;
}

//Bustabit
function crashPointFromSeed(serverSeed, weight) {
    //let clientSeed = '0000000000000000041f35ca2d5252873819007e534984388b3742cc442c05a9';
    let clientSeed = '00000000000000000002478d26d3763f0e3f7ab17ffa5c081c1219cd0ba4fcf0'; // btc.com - block 707712
    let hash = crypto.createHmac('sha256', serverSeed).update(clientSeed).digest('hex');

    // Use the most significant 52-bit from the hash to calculate the crash point
    let h = parseInt(hash.slice(0, 52 / 4), 16);
    let e = Math.pow(2, 52);

    let result = Math.floor(((weight * e) - h) / (e - h));
    let crash_point = (result / 100).toFixed(2);
    crash_point = Math.max(1.10, crash_point);

    //max
    crash_point = Math.min(max_crash, crash_point);
    return crash_point;
}

//Paka
function crashPointFromHashPaka(seed, weight) {
    // 00000000000000000000c9bfecba6c7e060beefc862690f5726e63e0454181de -- high
    // 00000000000000000000c9bfecba6c7e060beefc862690f5726e63e0454181d5 -- medium
    // 000000000000000000000d32dca12a1da4d16b029f50524de9759799b522a0d4 -- low
    // 00000000000000000000cd2bcb44f656649c69d8b17ade0399168f3cbe859d26 -- paka

    const PAKA_SALT = '00000000000000000000c9bfecba6c7e060beefc862690f5726e63e0454181d5';
    const nBits = 52; // number of most significant bits to use

    // 1. HMAC_SHA256(message=seed, key=salt)
    const hash = HmacSHA256(seed, PAKA_SALT).toString();

    // 2. r = 52 most significant bits
    const r = parseInt(hash.slice(0, nBits / 4), 16);

    // 3. X = r / 2^52
    const X = r / Math.pow(2, nBits); // uniformly distributed in [0; 1)

    // 4. X = 90 / (1-X)
    const result = Math.floor(weight / (1 - X)); //Orig is 90 - Dan | Paka - 93 | high a bit - 100

    // 5. return capped to 1 on lower end
    let crash_point = Math.max(1, result / 100);

    //Max
    crash_point = Math.min(max_crash, crash_point);
    return crash_point;
}

//BC Games
function crashPointFromHashBC(seed, weight = 99) {
    //const SALT = '0000000000000000000e3a66df611d6935b30632f352e4934c21059696117f28'; //btc.com - block 635380
    //const SALT = '00000000000000000002478d26d3763f0e3f7ab17ffa5c081c1219cd0ba4fcf0'; // btc.com - block 707700
    const SALT = '00000000000000000005a6ced2ca36d5a2f7790a87fd368ff01baf9e867c07cd' //btc.com - block 700777

    const nBits = 52; // number of most significant bits to use

    // 1. HMAC_SHA256(message=seed, key=salt)
    //const hmac = HmacSHA256(HEX.parse(seed), SALT).toString(HEX);
    const hmac = HmacSHA256(seed, SALT).toString();

    // 2. r = 52 most significant bits
    const r = parseInt(hmac.slice(0, nBits / 4), 16);

    // 3. X = r / 2^52
    let X = r / Math.pow(2, nBits); // uniformly distributed in [0; 1)
    X = parseFloat(X.toPrecision(9));

    // 4. X = 99 / (1-X)
    X = weight / (1 - X); //default 99

    // 5. return max(trunc(X), 100)
    const result = Math.floor(X);
    let crash_point = Math.max(1, result / 100);

    //Max
    crash_point = Math.min(max_crash, crash_point);
    return crash_point;
}

/*
 * Calculate Winning
 */
Random.calculateWinning = function (amount, timeStart, isHuman) {
    let ts = new Date();
    let rate = Math.pow(Math.E, 6e-5 * (ts - timeStart)).toFixed(2);

    if (isHuman === true) {
        rate = timeStart;
    }

    let winning = amount * (rate - 1);
    return {cashout: rate, won: winning};
}

Random.calculateTrenWinning = function (amount, button, crashBustNum) {
    let rate = 0;
    switch (button) {
        case 'red':
            if (crashBustNum <= 1.99) { //crashBustNum>= 1.1 &&
                //red won
                rate = 1.70;
            }
            break;
        case 'green':
            if (crashBustNum >= 2) {
                //red won
                rate = 2;
            }
            break;
        case 'moon':
            if (crashBustNum >= 10) {
                //red won
                rate = 10;
            }
            break;
    }

    if (rate !== 0) {
        let winning = amount * (rate - 1);
        return {cashout: rate, won: winning};
    }

    return false;
}

Random.calculateTrenCashout = function (button) {
    let player_cashout = 100;
    switch (button) {
        case 'red':
            player_cashout = 199;
            break;
        case 'green':
            player_cashout = 200;
            break;
        case 'moon':
            player_cashout = 1000;
            break;
    }

    return player_cashout;
}

/*
 * calculate Timeout
*/
Random.calculateTimeout = function (crash_num) {
    return (Math.log(crash_num) / Math.log(Math.E)) / 6e-5;
}

// Export the Random module
module.exports = Random;