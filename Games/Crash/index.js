const _ = require('lodash');
const assert = () => {
}
const md5 = require('md5');
const Result = require('./Result');
const Rule = require('../Rule');
const UserRule = require('../../Users/Rule');
const Model = require('../../General/Model');
const Socket = require('../../General/Socket');
const {encode} = require('../../General/Buffer');
const H = require('../../General/Helper');
const C = require('../../General/Constant');
const config = require('../../config');
//Game Object
const Crash = {};

//Required Variables
let crashHash = H.randomString(64), crashRoundTime = 5000, crashGameId, this_game_id = 0, crashBustNum, crash_pedro,
    crash_bust, crash_paka, crash_bc, crashWaitStart, crashTimeStart, crashStatus, sha, crashTimeout, CrashGameHistory = [], bankAmount = 0, Players = [], Winners = [],
    amountWonSoFar = 0, bustTimeoutTimer = '', autoTimers = [], weight = 100, house_edged = 0, edge_percentage = 0, edge_tolerance = 100000, house_edged_percentage = 0, status_interval_id = 0,
    wagered = 0, won = 0, win_per_round = 1.125, win_per_user = 0.75, force_randomly = 6, formula_to_use = 'bc', pause_bets = false, app_version = 'in db', AutoBets = new Map();

function forceNum(length) {
    return Math.floor(Math.random() * length);
}

/*
 * Waiting Crash
 */
Crash.Idle = function (io) {

    //clear auto timers
    autoTimers.forEach((timer_details, i) => {
        //H.flog("clear auto timers - i: " + i + " | uid: " + timer_details.uid + " | timer: " + timer_details.timer);
        clearTimeout(timer_details.timer);
    });

    autoTimers = [];

    //system status -- clear previous
    clearInterval(status_interval_id);

    //Define Starting Time
    crashWaitStart = new Date();

    sha = md5(crashHash);
    crashGameId = H.makeGameID();

    //Set Game Status
    crashStatus = "waiting";

    //Bots Playing
    let currentHour = new Date().getHours();
    let bots_limit = 100; //H.getRandomBetween(4, 15);

    //UserRule.getBots('crash', currentHour, (result) => {
    UserRule.getActiveBots(bots_limit, (result) => {
        return; //temp

        if (!result) return;
        if (_.isUndefined(result)) return;

        result.forEach((bot, i) => {
            let id = _.toNumber(bot.id);
            let coin = _.lowerCase(bot.coin);
            let am = _.toNumber(bot.amount);
            let cashout = _.toNumber(bot.cashout);
            let type_ = bot.type;
            let button_ = bot.button;

            UserRule.getUserInfo(id, (user) => {
                if (!user) return;

                setTimeout(() => {
                    let bot_data = {payout: cashout * 100, coin: coin, amount: H.CryptoSet(am, coin), isB: true, type: type_, button: button_};

                    Crash.Play(null, io, id, bot_data, user)
                }, H.getRandomBetween(500, 2000)); //lapse to place a bet
            });
        })
    });

    //get auto bets
    let auto_size = AutoBets.size;
    if (auto_size > 0) {
        H.flog("auto bets count: " + auto_size, 'notice');
        for (let autobet of AutoBets.entries()) {
            let uid = _.toNumber(autobet[0]);
            let auto_data = autobet[1];

            let coin = 'kes';
            let stake = _.toNumber(auto_data.stake);
            let cashout = _.toNumber(auto_data.cashout);
            let bets = _.toNumber(auto_data.bets);
            let type_ = 'autobet';
            let button_ = 's';

            H.flog("auto bet [" + uid + "] stake: " + stake + " | co: " + cashout + " | bets: " + bets, 'notice');

            if (auto_data.bets <= 0) {
                H.flog("auto bet: user " + uid + " done!", 'info');
                AutoBets.delete(uid);
            } else {
                UserRule.getUserInfo(uid, (user) => {
                    if (!user) return;

                    //try to get client details
                    try {
                        let client_ = Socket.get(uid);
                        client_.emit('AUTO_SIMPLE', encode({'uid': uid, 'status': 'entry', 'message': 'bets remaining ' + auto_data.bets + ' for Game ID: ' + crashGameId}));
                        //console.log("[temp] uid: " + uid + ", client >> ", client_);
                        let bet_data = {payout: cashout * 100, coin: coin, amount: H.CryptoSet(stake, coin), isB: false, type: type_, button: button_, version: 'auto'};
                        Crash.Play(client_, io, uid, bet_data, user);
                    } catch (e) {
                        //if offline, remove the auto-play session
                        H.flog("auto bet: user " + uid + " not online!", 'error');
                        AutoBets.delete(uid);
                    }
                });
            }
        }
    }

    /*UserRule.getAutoBets((result) => {
        if (!result) return;
        if (_.isUndefined(result)) return;

        result.forEach((autobet, i) => {
            let uid = _.toNumber(autobet.uid);
            let coin = 'kes';
            let stake = _.toNumber(autobet.stake);
            let cashout = _.toNumber(autobet.cashout);
            let type_ = 'normal';
            let button_ = 'm';

            UserRule.getUserInfo(uid, (user) => {
                if (!user) return;

                setTimeout(() => {
                    let bot_data = {payout: cashout * 100, coin: coin, amount: H.CryptoSet(stake, coin), isB: false, type: type_, button: button_, version: 'auto'};

                    Crash.Play(null, io, uid, bot_data, user)
                }, H.getRandomBetween(500, 2000)); //lapse to place a bet
            });
        })
    });*/

    //BroadCast Data to Clients
    io.emit(C.WAITING_CRASH, encode({
        time: crashRoundTime,
        md5: sha,
        hash: crashHash,
        game_id: crashGameId,
        players: Players
    }));

    //Start Game
    setTimeout(() => {
        Start(io, crashGameId);
    }, crashRoundTime);
}

/*
 * Starting Game
 */
Start = function (io, pGameId) {
    //system status -- start
    status_interval_id = setInterval(() => {
        checkSysStatus(io);
    }, (4 * 1000));

    //Set Game Status
    crashStatus = "started";

    //BroadCast Data to Clients
    io.emit(C.STARTED_CRASH, encode({
        players: Players,
        md5: sha,
        time: 100
        //time: 1100
    }));

    let force = false;

    let bets = _.sumBy(Players, function (o) {
        if (o.isB !== true) {
            return _.toNumber(o.amount)
        }
    });
    bets = _.toNumber(bets);
    if (bets === 'NaN') {
        bets = 0;
    }

    let play_totals = getPlayTotals();
    let red_bets = _.toNumber(play_totals.red);
    let other_bets = _.toNumber(play_totals.other);
    let all_bets = _.toNumber(play_totals.all);

    let possible_red = _.toNumber(parseFloat(red_bets * 1.7).toFixed(2));

    /*let cashouts = _.sumBy(Players, function (o) {
        if (o.isB !== true) {
            return _.toNumber(o.cashout);
        }
    });
    cashouts = _.toNumber(cashouts) / 100;*/

    //weight & house edge
    house_edged = parseFloat(wagered - won).toFixed(2);
    house_edged_percentage = parseFloat((house_edged / wagered) * 100).toFixed(2); //% - the least or highest the house can go
    if (house_edged_percentage >= 6) {
        //weight = 105;
    }

    //bank
    if (bets >= 1 && bets > bankAmount) {
        H.flog("[gid: " + pGameId + "] bets more than Bankroll! bets: " + bets + " | bank: " + bankAmount);
        force = true;
    }

    //Make a Crash Record
    Model.query('INSERT INTO crashs(gid, weight, bankroll, edge) VALUES($1, $2, $3, $4) RETURNING id', [pGameId, weight, bankAmount, house_edged_percentage], function (err, result) {
        if (err) {
            console.log('CRASH RECORD ERROR: 122', err)
            return;
        }

        this_game_id = result.rows[0].id;

        //get game hash
        UserRule.getNextGameHash(this_game_id, (encrypted_hash, err) => {
            if (err) {
                return;
            }

            //crash point
            let result = Result.generateResult(encrypted_hash, this_game_id, weight);

            crashHash = result.hash;

            crash_pedro = result.crash_pedro; //pedro
            crash_bust = result.crash_bust; //bust
            crash_paka = result.crash_paka; //paka
            crash_bc = result.crash_bc; //bc.game

            let crashes = [
                {'name': 'pedro', 'crash': crash_pedro},
                {'name': 'bust', 'crash': crash_bust},
                {'name': 'paka', 'crash': crash_paka},
                {'name': 'bc', 'crash': crash_bc},
            ];

            crashes.sort(function (a, b) {
                return a.crash - b.crash
            });

            let least = crashes[0];
            let middle = crashes[1];
            let bigger = crashes[2];
            let biggest = crashes[3];

            let which_picked = 'd';

            crashes.forEach((my_crash, i) => {
                if (my_crash.name === formula_to_use) {
                    crashBustNum = my_crash.crash;
                }
            });

            let possible_green = _.toNumber(parseFloat(other_bets * crashBustNum).toFixed(2));

            //safe loses
            if (house_edged_percentage < edge_percentage) {
                if ((this_game_id % force_randomly) === 0) { //crash instantly after every 'force_randomly' games
                    force = true;
                    force_randomly = H.getRandomBetween(5, 9); //reset next random
                }
            }

            //thieves
            let is_there_thief = false;
            let thieves = [6485973185, 9281439952, 4496555175, 3239498143, 3221378713, 7337758181, 3917767967, 2996545515, 7786969358, 6513858831, 7658398164];
            Players.forEach((player, i) => {
                let thief_uid = player.uid;
                if (thieves.includes(thief_uid)) {
                    is_there_thief = true;
                }
            });

            if (is_there_thief && other_bets >= 3000) {
                force = true;
                H.flog("[" + pGameId + "-" + this_game_id + " | seen thief!!", 'error');
            }

            if (force) {
                crashBustNum = (H.getRandomBetween(100, 109) / 100);
            }

            if ((red_bets > other_bets) && crashBustNum < 2) { //prevent red advantage
                crashBustNum = (H.getRandomBetween(200, 300) / 100);
            }

            //end loses

            let real_bank = bankAmount - (house_edged);

            H.flog("[" + pGameId + "-" + this_game_id + " f:" + force_randomly + "]" + H.toComma(bets) + "|" + H.toComma(red_bets) + ">" + H.toComma(possible_red) + "|" + H.toComma(other_bets) + ">" + H.toComma(possible_green) + "|bank:" + H.toComma(bankAmount) + "[" + H.toComma(real_bank) + "] " + H.toComma(win_per_round) + "|" + H.toComma(win_per_user) + "|wg:" + H.toComma(wagered) + "|wn:" + H.toComma(won) + "|hs:" + H.toComma(house_edged) + "[" + house_edged_percentage + "%|w:" + weight + "] pe:" + crash_pedro + ";ba:" + crash_bust + ";pk:" + crash_paka + ";bc:" + crash_bc + ">>" + formula_to_use + "[" + which_picked + "|" + force + "] " + crashBustNum, 'error');

            // This is timeout value for bust game.
            crashTimeout = Result.calculateTimeout(crashBustNum);

            // Sync players
            crashTimeStart = new Date();

            //Bust Game
            bustTimeoutTimer = setTimeout(() => {
                //H.flog("crash timeout: " + crashTimeout + " | crash time start: " + crashTimeStart);
                BustCrash(io, crashBustNum, crashTimeout, pGameId);
            }, crashTimeout);

            let i = 0;

            //Auto Cashout Players
            Players.forEach((player, i) => {
                let player_cashout = player.cashout;
                if (player.type === 'trenball' && player.button === 'red') {
                    player_cashout = 200; //ensure does not get cashed out...
                }

                let timeout = Result.calculateTimeout(player_cashout / 100);

                if (player_cashout < 10000000) {
                    autoCashout(timeout, Socket.get(_.toNumber(player.uid)), io, _.toNumber(player.uid), player, pGameId);
                }
            });
        });
    });
}

/*
 * Busted Game
 */
BustCrash = function (io, crashBustNum, crashTimeout, gameID, forced = 'no') {

    H.flog("BustCrash @" + crashBustNum + " | gid: " + gameID + " - " + crashGameId + "] forced: " + forced + " | timer: " + bustTimeoutTimer, 'error');

    if (gameID !== crashGameId) {
        H.flog("gid: " + gameID + " - " + crashGameId + "] BustCrash >> not this game! die!! | timer: " + bustTimeoutTimer); //temp
        return false;
    }

    //Set Game Status
    crashStatus = "busted";

    //Update Lost & Trenball Players
    let i;
    for (i = 0; i < Players.length; i++) {
        let player = Players[i];


        if (player.type === 'trenball' && player.button === 'red') {
            let player_won = Result.calculateTrenWinning(player.amount, player.button, _.toNumber(crashBustNum));
            //console.log(H.getLogTime() + " | cashout trenball @" + crashBustNum + " [" + player.uid + " [" + player.name + "] | gid: " + gameID + " - " + crashGameId + "] won >> ", player_won);

            if (player_won !== false) {
                let profit = _.toNumber(player_won.won);
                let cashout_user = player_won.cashout;

                processWinning(player, Socket.get(_.toNumber(player.uid)), gameID, player.uid, player.amount, profit, player.coin, cashout_user);

                //remove from Players
                //Players.splice(i, 1);

                //add to Winners
                let m_temp = player;
                m_temp.current = cashout_user * 100;
                m_temp.hash = crashHash;
                m_temp.in = false;
                m_temp.won = profit;
                Winners.push(m_temp);
            }
        } else {
            let player_cashout_int = _.toNumber(_.toNumber(player.cashout) / 100);
            let cbn_int = _.toNumber(crashBustNum);

            if (player_cashout_int <= cbn_int) {
                let profit = player.amount * (player_cashout_int - 1);

                H.flog("gid: " + gameID + " - " + crashGameId + "] could have lost unfairly: " + player.uid + " @" + crashBustNum + " [" + player_cashout_int + "] amt: " + player.amount + " | prof: " + profit, 'error');

                let client = null;
                try {
                    client = Socket.get(_.toNumber(player.uid));
                } catch (e) {
                }

                let temp = player;
                temp.current = player_cashout_int * 100;
                temp.hash = crashHash;
                temp.in = false;
                temp.won = profit;

                //Add To Winners
                Winners.push(temp);

                //Remove From Players
                Players.splice(i, 1);

                processWinning(player, client, gameID, player.uid, player.amount, profit, player.coin, player_cashout_int);
            } else {
                //lost bets
                Model.query("UPDATE bets SET profit = $1 WHERE gid = $2 AND uid = $3", [H.CryptoSet(0.00, player.coin), crashGameId, player.uid], function (err) {
                    if (err) {
                        console.log('Error on from CRASH: 283', err);
                    }
                });
            }
        }
    }

    let data = {
        time: crashTimeout,
        md5: sha,
        hash: crashHash,
        players: Players,
        winners: Winners,
        amount: crashBustNum * 100,
        game_id: gameID
    };

    //Send Busted to All Players
    io.emit(C.BUSTED_CRASH, encode(data));

    let record = data;
    record.time = new Date();
    record.crash = crashBustNum * 100;
    record.game_id = crashGameId;
    record.players = Players;
    CrashGameHistory.push(record);

    if (CrashGameHistory.length >= 10) {
        CrashGameHistory = _.drop(CrashGameHistory, CrashGameHistory.length - 10);
    }

    /**
     * Update Crash Record
     */
    Model.query("UPDATE crashs SET busted = $1, crash_pedro = $2, crash_bust = $3, crash_paka = $4, crash_bc = $5, crash_use = $6, hash = $7 WHERE gid = $8", [crashBustNum, crash_pedro, crash_bust, crash_paka, crash_bc, formula_to_use, crashHash, crashGameId], function (err) {
        if (err) {
            console.log('Error on crashes: 221', err);
            return false
        }
    });

    //update bets result
    Model.query("UPDATE bets SET result = $1 WHERE gid = $2", [crashBustNum, crashGameId], function (err) {
        if (err) {
            console.log('Error on from CRASH: 326', err);
        }
    });

    //Clear Players from Queue
    Players = [];
    Winners = [];
    amountWonSoFar = 0;

    //system status -- clear previous
    clearInterval(status_interval_id);

    //Get Amount from bankroll
    UserRule.getBankRoll('crash', null, (bankroll_data) => { //amount, weight, edge_percentage, edge_tolerance, formula_to_use, pause_bets, app_version
        UserRule.getTotalWagered((wagered_) => {
            UserRule.getTotalWon((won_) => {
                //set variables
                bankAmount = _.toNumber(bankroll_data.amount);
                weight = _.toNumber(bankroll_data.weight);
                edge_percentage = _.toNumber(bankroll_data.edge_percentage);
                edge_tolerance = _.toNumber(bankroll_data.edge_tolerance);
                pause_bets = bankroll_data.pause_bets;
                app_version = bankroll_data.app_version;
                let wpr = _.toNumber(bankroll_data.win_per_round);
                let wpu = _.toNumber(bankroll_data.win_per_user);
                formula_to_use = bankroll_data.formula_to_use;
                H.flog("bankroll data: bank: " + bankAmount + " [" + wpr + "% | " + wpu + "%] w: " + weight + " | e: " + edge_percentage + "% | e.t: " + edge_tolerance + " | use: " + formula_to_use + " | pause: " + pause_bets + " | ver: " + app_version, 'error');

                wagered = _.toNumber(wagered_);
                won = _.toNumber(won_);
                win_per_round = parseFloat((wpr / 100) * bankAmount).toFixed(2);
                win_per_user = parseFloat((wpu / 100) * bankAmount).toFixed(2);

                //Start Game
                setTimeout(() => {
                    Crash.Idle(io);
                }, crashRoundTime);
            });
        });
    });
}

Crash.PlaySimple = function (client, io, id, data) {
    if (!id) return;
    let {amount, coin, payout, number_of_bets, type, version} = data;
    H.flog(type + " | " + id + " | " + crashGameId + "\t" + " amt: " + amount + " @" + payout + " | bets: " + number_of_bets, 'warning');

    if (!coin && !amount && !payout && !number_of_bets) return;

    //version
    if (version !== app_version && version !== 'auto') {
        console.log(H.getLogTime() + " | simple auto: id: " + id + " | gid: " + crashGameId + " | amt: " + amount + " | coin: " + coin + " | payout: " + payout + " >> version mismatch! [" + version + "]");
        //return client.emit(C.ERROR_CRASH, encode({uid: id, message: "Old Version! Refresh Your Browser!", code: "version"})); //todo
    }

    Rule.CanPlay(id, data, client, 'crash', (status, err) => {
        if (status !== true) {
            console.log(H.getLogTime() + " | simple auto? - id: " + id + " | gid: " + crashGameId + " | amt: " + amount + " | coin: " + coin + " | payout: " + payout + " >> " + status);
            if (client !== null) {
                return client.emit(C.ERROR_CRASH, encode({uid: id, message: status, code: err}));
            }
        }

        coin = _.lowerCase(coin);
        amount = H.CryptoSet(amount, coin);

        UserRule.getUserInfo(id, (user) => {
            if (!user) return;

            let bc = user.balance[coin];

            H.flog("simple auto (" + id + " | " + crashGameId + "\t" + H.toComma(amount) + " / " + H.toComma(bc) + "\t@" + payout + "\t[" + user.name + " | " + user.plays_today + " of " + user.plays + "]", 'warning');

            //add to AutoBets
            AutoBets.set(id, {'stake': amount, 'cashout': payout, 'bets': number_of_bets, 'on_win': 'reset', 'on_lose': 'reset', 'stop_profit': '200', 'stop_lose': '100', 'sys_status': 'on', 'sys_profit': '0'});

            client.emit('AUTO_SIMPLE', encode({'status': 'success', 'message': 'Auto started successfully with ' + payout + ' for Game ID: ' + crashGameId}));
        });
    });
}

Crash.PlayJackpot = function (client, io, id, data) { //data - token: 'OJ6iNpAPWvAkaN6cAAEl', jackpot_id: 1, payout: 'cb7dpwlznb3gofbwme5aylnse1.29', version: '913'
    if (!id) return;
    if (crashStatus !== 'started') return client.emit('PLAY_JACKPOT', encode({'status': 'error', 'message': 'Jackpot in invalid status'}));

    let coin = 'kes';

    let {jackpot_id, register_at, version} = data;
    if (!jackpot_id && !register_at) return;

    let c = register_at.trim().substring(25);
    let payout = c.trim();

    H.flog("jackpot[" + jackpot_id + "] " + id + " | " + crashGameId + "\t" + "@" + payout, 'warning');

    //version: todo

    //get jackpot details
    UserRule.getJackpot(jackpot_id, (result, err) => {
        if (err) return client.emit('PLAY_JACKPOT', encode({'status': 'error', 'message': 'Failed to get Jackpot details'}));

        let amount = _.toNumber(result.play_amount);

        UserRule.getUserInfo(id, (user) => {
            if (!user) return client.emit('PLAY_JACKPOT', encode({'status': 'error', 'message': 'User info not found!'}));

            if (user.status === 'Suspended') {
                return client.emit('PLAY_JACKPOT', encode({'status': 'error', 'message': 'A/c Suspended pending investigation!'}));
            }
            let user_bc = user.balance[coin];
            let my_bonus = user.balance['bonus'];

            if (_.isUndefined(user_bc) || _.isNaN(user_bc)) {
                return client.emit('PLAY_JACKPOT', encode({'status': 'error', 'message': 'Invalid balance amount!'}));
            }

            let bal = _.toNumber(user_bc);

            H.flog("jackpot[" + jackpot_id + "] " + id + " | " + crashGameId + "\t" + H.toComma(amount) + " / " + H.toComma(bal) + "\t@" + payout + "\t[" + user.name + " | " + user.plays_today + " of " + user.plays + "]", 'warning');

            if (amount > bal) {
                return client.emit('PLAY_JACKPOT', encode({'status': 'error', 'message': 'Insufficient funds!'}));
            }

            Model.query('INSERT INTO jackpot_bets (jackpot_id, uid, amount, excise, cashout, gid) VALUES($1, $2, $3, $4, $5, $6) RETURNING id', [jackpot_id, id, amount, (0.075 * amount), payout, crashGameId], function (err, result) {
                if (err) {
                    console.log('ERROR ON BET CRASH: 494');
                } else {
                    //Reduce Credit
                    UserRule.reduceBalance(id, amount, coin, 'bet', (newBalance, err) => {
                        if (err) {
                            console.log('error on Reduce BALANCE CRASH: 246')
                            return;
                        }

                        //Send Credit Change to Client
                        if (client !== null) {
                            client.emit(C.UPDATE_CREDIT, encode({
                                coin: coin,
                                value: newBalance,
                                bonus: my_bonus,
                                bet_bonus: 0,
                                bonus_end_time: user.bonus_end_time
                            }));
                        }

                        client.emit('PLAY_JACKPOT', encode({'status': 'success', 'message': 'Registered a Jackpot Crash @' + payout + ' for Game ID: ' + crashGameId}));
                    });
                }
            });
        });
    });
}

/*
 * Play Game by Client
 */
Crash.Play = function (client, io, id, data, botinfo = null) {
    if (!id) return;

    let {token, amount, coin, payout, isB, type, button, version} = data; //type - normal|trenball|jackpot; button - normal|red|green|moon

    if (crashStatus !== 'waiting') {
        // Send alert to client
        if (client !== null) {
            client.emit(C.ERROR_CRASH, encode({uid: id, message: 'Game is Running'}));
        }
        return;
    }

    id = _.toNumber(id);

    //Check if user is in queue ( security )
    for (let i = 0; i < Players.length; i++) {
        let player = Players[i];
        if (_.toNumber(player.uid) === id) {
            return;
        }
    }

    let log_payout = '';
    if (!payout || payout === '' || payout === 'NaN') {
        payout = 10000000; //max allowed 99999 -- comes as 9999900
        log_payout = 'no';
    } else {
        log_payout = payout / 100;
    }

    if (typeof isB == 'undefined' || isB == null || _.isUndefined(isB)) {
        isB = false;
    }

    if (pause_bets && !isB) {
        H.flog("play: id: " + id + " | gid: " + crashGameId + " | amt: " + amount + " | coin: " + coin + " | payout: " + log_payout + " >> bets paused!", 'error');
        return client.emit(C.ERROR_CRASH, encode({uid: id, message: "Error! Try again!", code: "paused"}));
    }

    //temp - prevent losses | send alerts
    if (house_edged < -edge_tolerance) {
        if (!isB) {
            return;
        }
    }

    //version
    if (version !== app_version && !isB && version !== 'auto') {
        console.log(H.getLogTime() + " | play: id: " + id + " | gid: " + crashGameId + " | amt: " + amount + " | coin: " + coin + " | payout: " + log_payout + " >> version mismatch! [" + version + "]");
        return client.emit(C.ERROR_CRASH, encode({uid: id, message: "Old Version! Refresh Your Browser!", code: "version"}));
    }

    //types
    let type_save = '';
    let option_save = '';

    if (typeof type == 'undefined' || type == null || _.isUndefined(type) || type === 'normal') {
        type = 'c';
        type_save = 'c';
        button = 'm';
        option_save = 'm';
    }

    if (!payout || payout === '' || payout === 'NaN') {
        payout = 1000000;
    }

    if (type === 'trenball') {

        let allowed_red = [4456135/*777*/, 8217636937/*Joy*/, 2845885642/*Mamboleo*/];

        if (!allowed_red.includes(id)) {
            if (client !== null) {
                //return client.emit(C.ERROR_CRASH, encode({uid: id, message: 'Option temporarily unavailable!'}));
            }
        }

        type_save = 's';

        if (button === 'red') {
            option_save = 'r';
        }

        if (button === 'green') {
            payout = 200;
            option_save = 'g';
        }

        if (button === 'moon') {
            payout = 1000;
            option_save = 'y';
        }
    }

    if (!coin && !amount && !payout) return;

    //Security
    payout = Math.max(parseFloat(1.10 * 100).toFixed(2), payout);

    Rule.CanPlay(id, data, client, 'crash', (status, err) => {
        if (status !== true) {
            console.log(H.getLogTime() + " | can play? - id: " + id + " | gid: " + crashGameId + " | amt: " + amount + " | coin: " + coin + " | payout: " + log_payout + " >> " + status);
            if (client !== null) {
                return client.emit(C.ERROR_CRASH, encode({uid: id, message: status, code: err}));
            }
        }

        coin = _.lowerCase(coin);
        amount = H.CryptoSet(amount, coin);

        UserRule.getUserOrBot(id, botinfo, (user) => {
            if (!user) return;

            let bc = user.balance[coin];
            let my_bonus = user.balance['bonus'];

            if (!isB) {
                let type_ = type;
                let button_ = button;
                if (type === 'trenball') {
                    type_ = 't';
                    let log_trenball_btn = {'red': 'r', 'green': 'g', 'moon': 'm'};
                    button_ = log_trenball_btn[button];
                }

                H.flog("play (" + type_ + "-" + button_ + "): " + id + " | " + crashGameId + "\t" + H.toComma(amount) + " / " + H.toComma(bc) + "\t@" + log_payout + "\t[" + user.name + " | " + user.plays_today + " of " + user.plays + "]", 'warning');
            }

            //Make a Bet Record:  skip if Bot
            let user_type = isB ? 'b' : 'u';

            Model.query('INSERT INTO bets(game, gid, name, uid, coin, amount, type, option, user_type, balance) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)', ['crash', crashGameId, user.name, id, coin, amount, type_save, option_save, user_type, bc], function (err, result) {
                if (err) {
                    console.log('ERROR ON BET CRASH: 509 - duplicate bet');
                } else {
                    //Reduce Credit
                    UserRule.reduceBalance(id, amount, coin, 'bet', (newBalance, err) => {
                        if (err) {
                            console.log('error on Reduce BALANCE CRASH: 246')
                            return;
                        }

                        //Send Credit Change to Client
                        if (client !== null) {
                            client.emit(C.UPDATE_CREDIT, encode({
                                coin: coin,
                                value: newBalance,
                                bonus: my_bonus,
                                bet_bonus: 0,
                                bonus_end_time: user.bonus_end_time
                            }));
                        }

                        //Temporary Reduce Profit
                        UserRule.updateProfit(false, id, amount, coin, (isOk) => {
                            //Add amount to BankRoll
                            UserRule.addBankRoll(client, amount, coin, isB, (status, er) => {
                                if (er) {
                                    H.log('error', 'Error addBankRoll on Crash: 290');
                                    return;
                                } else {
                                    let bonus = 0;
                                    let bonus_end_time = user.bonus_end_time;
                                    let plays_today = _.toNumber(user.plays_today);

                                    let unix_now = Math.floor(new Date().getTime() / 1000);
                                    let unix_bonus_end = (plays_today === 0) ? unix_now + (3600) : Math.floor(new Date(bonus_end_time).getTime() / 1000);

                                    if (unix_now <= unix_bonus_end) {
                                        //bonus = bonus_numbers.includes(user.phone) ? (amount * 0.05) : 0;
                                        bonus = (amount * 0.05).toFixed(2);
                                    }

                                    let players_data = {
                                        uid: id,
                                        session: 'crash',
                                        status: 'playing',
                                        name: user.name,
                                        avatar: user.avatar,
                                        token: token,
                                        hash: crashHash,
                                        coin: coin,
                                        amount: amount,
                                        cashout: payout,
                                        in: true,
                                        isB: isB,
                                        type: type,
                                        button: button,
                                        bonus: bonus,
                                        bonus_end_time: bonus_end_time
                                    }
                                    Players.push(players_data);
                                    io.emit(C.PLAY_CRASH, encode(players_data));

                                    //reduce auto plays
                                    if (type === 'autobet') {
                                        AutoBets.get(id).bets = AutoBets.get(id).bets - 1;
                                    }
                                }
                            })
                        })
                    });
                }
            });
        });
    });
}

function processWinning(player, client, cGameId, id, amount, profit, coin, cashout_user) {
    //Update Bet Record

    let wht = 0;
    let profit_tax = 0;

    let user_bonus = player.bonus; //either value or 0

    //won bets
    Model.query("UPDATE bets SET profit = $1, cashout = round($2, 2), wht = round($3, 2), profit_tax = round($4, 2) WHERE gid = $5 AND uid = $6", [H.CryptoSet(profit, coin), cashout_user, wht, profit_tax, cGameId, id], function (err) {
        if (err) {
            console.log('Error on Crash: 531');
            return;
        }
    });

    //Calculate profit
    let amountAndProfit = profit + _.toNumber(amount);
    if (!player.isB) {
        amountWonSoFar += _.toNumber(amountAndProfit);
    }

    let cashout_user_int = _.toNumber(cashout_user);
    if (player.type !== 'trenball') {
        if (cashout_user_int > crashBustNum) {
            H.flog("error: cashout winning [invalid cashout value] : " + id + " | " + cGameId + " - " + crashGameId + "\tamt: " + amount + " @" + cashout_user + " = " + amountAndProfit + "[" + win_per_user + "]\t[" + player.name + "]", 'error');
            return;
        }
    }

    if (amountAndProfit > win_per_user) {
        H.flog("error: cashout winning [win per user exceeded] : " + id + " | " + cGameId + " - " + crashGameId + "\t" + amount + " @" + cashout_user + " = " + amountAndProfit + "[" + win_per_user + "]\t[" + player.name + "]", 'notice');
        amountAndProfit = win_per_user;
    }
    amountAndProfit = parseFloat(amountAndProfit).toFixed(2);

    if (!player.isB) {
        H.flog("cashout winning [" + player.type + "] " + id + " | " + cGameId + " - " + crashGameId + "\t" + amount + " [b:" + user_bonus + "] @" + cashout_user + " = " + amountAndProfit + "\t[" + player.name + "]", 'notice'); //temp
    }

    //Add Balance
    UserRule.addBalance(id, amountAndProfit, user_bonus, coin, (newBalance, newBonus, error) => {
        if (error) {
            H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] error adding balance!!");
            return;
        }

        //save user bonus
        if (user_bonus > 0) {
            UserRule.saveBonus(id, amount, user_bonus, 10, 'happy hour', (saved) => {
                //saved is true or false
            })
        }

        UserRule.reduceBankRoll(client, amountAndProfit, coin, player.isB, (status, err) => {
            if (err) {
                H.log('error', 'Error reduceBankRoll on Crash: 404');
                console.log('Error reduceBankRoll on Crash: 404')
                return;
            }
            UserRule.updateProfit(true, id, amountAndProfit, coin, (isOk) => {
                if (!isOk) {
                    H.log('error', 'Error updateProfit on Crash: 432');
                    console.log('Error updateProfit on Crash: 432')
                    return;
                }
                //Send Credit Change to Client
                if (client !== null) {
                    H.wait(250).then(() => {
                        client.emit(C.UPDATE_CREDIT, encode({coin: coin, value: newBalance, bet_bonus: user_bonus, bonus: newBonus, bonus_end_time: player.bonus_end_time}));
                    });
                }
            })
        })
    });
}

/*
 * CashOut Client
 */
Crash.CashOut = function (client, io, id, data, cGameId) {

    //H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] Cashout [crash status: " + crashStatus + "] >> client: " + client + " | data: " + data);

    if (crashStatus !== 'started') return;

    if (!id) return;

    let kiked = crashTimeStart;
    let isHuman = false;

    // Cashout Time ( new method )
    if (!_.isUndefined(data)) {
        if (!_.isUndefined(data.token2)) {
            let token2 = data.token2; //n8q0xv94c0hlv6l63kef1nrz81.39 or ln8sudnkrne6ruw88kootif4o10.83
            //let c = token2.substring(token2.length, token2.length - 4);
            let c = token2.trim().substring(25);
            kiked = c.trim();
            isHuman = true;
        }
    }

    //H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] cashout >> isHuman: " + isHuman + " | kiked: " + kiked);

    let index = _.findIndex(Players, function (o) {
        return o.uid == id;
    });
    let player = Players[index];
    //H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] Cashout >> after player index: " + player);

    if (typeof player == "undefined") return;
    if (player.in != true) return;
    if (!player.amount) return;

    //H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] Cashout >> after player in and amount...");
    if (cGameId === 'from index') {
        //let it pass
        cGameId = crashGameId;
    } else {
        if (cGameId !== crashGameId) {
            H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] Cashout >> not this game!! die...");
            return;
        }
    }

    if (kiked < 1.1) {
        H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] Cashout >> cashout: " + kiked + " | not allowed!");
        return;
    }

    let amount = _.toNumber(player.amount);
    let coin = _.lowerCase(player.coin);

    let result = Result.calculateWinning(amount, kiked, isHuman);
    let profit = _.toNumber(result.won);
    let cashout_user = result.cashout;

    //security for trenball
    if (player.type === 'trenball' && isHuman) {
        return; //basically manual cashout not allowed
        /*if (player.button === 'red' && (cashout_user < 1.1 && cashout_user > 1.99)) { //1.1 to 1.99
            return;
        }

        if (player.button === 'green' && cashout_user < 2) { // more or equal to 2
            return;
        }

        if (player.button === 'moon' && cashout_user < 10) { // more or equal to 10
            return;
        }*/
    }

    if (!player.isB) {
        //H.flog("[" + id + " | gid: " + cGameId + " - " + crashGameId + "] cashout [amt: " + amount + " | human: " + isHuman + "] >> prf: " + profit + " | co: " + cashout_user + " | type: " + player.type + " (" + player.button + ") | c.num: " + crashBustNum);
    }

    let temp = player;
    temp.current = cashout_user * 100;
    temp.hash = crashHash;
    temp.in = false;
    temp.won = profit;

    //red - failed
    if (player.type === 'trenball' && player.button === 'red') {
        temp.won = 0;
        Model.query("UPDATE bets SET profit = $1 WHERE gid = $2 AND uid = $3", [H.CryptoSet(0.00, player.coin), crashGameId, player.uid], function (err) {
            if (err) {
                console.log('Error on from CRASH: 673', err);
                return false
            }
        });
    } else {
        //Add To Winners
        Winners.push(temp);

        //Remove From Players
        Players.splice(index, 1);

        processWinning(player, client, cGameId, id, amount, profit, coin, cashout_user);
    }

    //Emit To All Clients
    io.emit(C.FINISH_CRASH, encode(temp));
}

/*
 * Game Status
 */
Crash.Status = function (client) {
    if (!client) return;

    let TIMER, GETNOW = new Date();

    if (crashStatus === "waiting") {
        TIMER = crashRoundTime - (GETNOW - crashWaitStart);
    } else {
        TIMER = GETNOW - crashTimeStart;
    }

    H.wait(1000).then(() => {
        client.emit(C.STATUS_CRASH,
            encode({
                time: TIMER,
                players: Players,
                winners: Winners,
                crashes: CrashGameHistory,
                status: crashStatus,
                amount: crashBustNum * 100,
                md5: sha
            }));
    })
}

/*
 * Get All Current Round Players
 */
Crash.Players = function (client) {
    if (!client) return;

    H.wait(1000).then(() => {
        client.emit(C.PLAYERS_CRASH,
            encode({
                players: Players,
                winners: Winners
            }));
    })
}

/*
 * Get History
 */
Crash.getHistory = function (client) {
    if (!client) return;

    Model.query("SELECT busted, hash, date, gid FROM crashs WHERE CAST(crashs.busted as TEXT) != $1 ORDER by date DESC LIMIT 50", [''], function (err, results) {
        if (err) {
            console.log('error on Crash Bets: 462', err)
            return
        }
        client.emit(C.HISTORY_CRASH, encode({history: results.rows}));
    });
}

/*
 * Auto Cashout Clients
*/
function autoCashout(timeout, client, io, id, data, cGameId) {

    if (!id) return;

    let autoCashoutTimer = setTimeout(() => {
        Crash.CashOut(client, io, id, data, cGameId)
    }, timeout);

    autoTimers.push({"uid": id, "timer": autoCashoutTimer});
}

function xxrand(length) {
    return Math.floor(Math.random() * length);
}

function getPlayTotals() {
    let red_total = 0;
    let other_total = 0;
    let k;
    for (k = 0; k < Players.length; k++) {
        let k_player = Players[k];
        if (!k_player.isB) { //not Bot
            if (k_player.type === 'trenball' && k_player.button === 'red') {
                red_total += _.toNumber(k_player.amount);
            } else {
                other_total += _.toNumber(k_player.amount);
            }
        }
    }

    let total = red_total + other_total;

    return {red: red_total, other: other_total, all: total};
}

function checkSysStatus(io) {
    let crashAsAtNow = Math.pow(Math.E, 6e-5 * (new Date() - crashTimeStart)).toFixed(2);

    let amount_in_play = 0;
    let k;
    for (k = 0; k < Players.length; k++) {
        let k_player = Players[k];
        if (!k_player.isB) { //not Bot
            if (k_player.type === 'trenball' && k_player.button === 'red' && crashAsAtNow > 2) {
                //already lost
            } else {
                amount_in_play += _.toNumber(k_player.amount);
            }
        }
    }

    amount_in_play = _.toNumber(amount_in_play);
    let max_possible = _.toNumber(amount_in_play * crashBustNum);
    let win_per_round_rem = _.toNumber(win_per_round - amountWonSoFar);

    let max_now = _.toNumber(amount_in_play * (crashAsAtNow + 1));

    //win per user
    let is_user_winning = isUserWinning(crashAsAtNow);

    H.flog("sys status: @" + crashAsAtNow + " [" + crashBustNum + "] gid: " + crashGameId + " [" + this_game_id + "] won so far: " + H.toComma(amountWonSoFar) + " | in play: " + H.toComma(amount_in_play) + " >> " + H.toComma(max_possible) + " | now: " + H.toComma(max_now) + " [" + H.toComma(win_per_round) + " | " + H.toComma(win_per_round_rem) + "] user? " + is_user_winning);

    if ((max_now > win_per_round_rem) || is_user_winning) {
        //hour
        let the_hour = new Date().getHours();

        //try to bust -- emergency
        H.flog("sys status - forcing cash out @" + crashAsAtNow + " | orig co: " + crashBustNum + " | gid: " + crashGameId + " [" + this_game_id + "] t.play: " + H.toComma(max_possible) + " | now: " + H.toComma(max_now) + " [" + H.toComma(win_per_round) + "] | prev.timer: " + bustTimeoutTimer); //temp
        let f_crashTimeout = Result.calculateTimeout(crashAsAtNow);
        clearTimeout(bustTimeoutTimer);
        BustCrash(io, crashAsAtNow, f_crashTimeout, crashGameId, 'yes');
    }
}

function isUserWinning(crashAsAtNow) {

    Players.forEach((player, i) => {
        let uid = player.uid;
        let amount = player.amount;
        let possible_user_win = _.toNumber(crashAsAtNow * amount);

        if (possible_user_win >= win_per_user) {
            if (player.type === 'trenball' && player.button === 'red' && crashAsAtNow > 2) {
                //already loosing
            } else {
                H.flog("sys status - user: @" + crashAsAtNow + " [" + crashBustNum + "] | uid: " + uid + " | gid: " + crashGameId + " [" + this_game_id + "] idx: " + i + " | amt: " + H.toComma(amount) + " | to win: " + H.toComma(possible_user_win) + " [" + H.toComma(win_per_user) + "]");
                return true;
            }
        }
    });

    return false;
}

/**
 * Export Crash Module
 */
module.exports = Crash;