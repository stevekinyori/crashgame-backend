const _ = require('lodash');
const UserRule = require('../Users/Rule');
const H = require('../General/Helper');
const Agent = require('./Agent');

//Decelare Rule Object
const Rule = {};

/*
 * Check User Can Play Game
 */
Rule.CanPlay = function (id, data, client, game, callback) {
    if (!id && !game) return;

    // If is Bot
    if (client === null)
        return callback(true);

    let {amount, coin} = data;

    if (!amount && !coin) return;

    id = _.toNumber(id);

    let bet = _.toString(amount);
    bet = _.replace(bet, ",", ".");
    bet = _.toNumber(bet)

    if (_.isNaN(bet)) {
        bet = parseFloat(bet);
    }

    if (bet === 0 && bet < 1e-7) {
        return callback(false);
    }

    if (!H.isValidNumber(bet)) {
        return callback(false);
    }

    if (bet < 10) return callback('Invalid bet amount!'); //maxbet - 3000

    // IMPORTANT SECURITY
    if (bet <= 0) return callback(false);
    if (bet === 0.00000000) return callback(false);
    if (bet <= 0.00000000) return callback(false);
    if (bet === '0.00000000') return callback(false);

    //Check BankRoll
    Agent.getBank(game, bet, coin, client, (can) => {
        if (!can) {
            return callback('Your Profit Can\'t be more than 5% of Bankroll!');
        }
        UserRule.getClientCoinCredit(id, coin, (result, err, user_status) => {
            if (result === false) {
                return callback('Insufficient funds!', 'credit');
            }

            if(user_status === 'Suspended') {
                return callback('A/c Suspended pending investigation!');
            }

            let clientBalance = _.toNumber(result);

            if (clientBalance >= bet) {
                callback(true)
            } else {
                callback('Insufficient funds!', 'credit')
            }
        })
    })
}

//Export Rule Module
module.exports = Rule;