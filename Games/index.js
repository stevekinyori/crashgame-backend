const _ = require('lodash');
const Token = require('../Users/Token');
const { decode } = require('../General/Buffer');
const C = require('../General/Constant');

//All Games Class
const Crash = require('./Crash')

/**
 * Game Handler
 */
function Game(client, io){
	/*
 	 * Get The Crash Status
	*/
	client.on(C.STATUS_CRASH, () => {
        Crash.Status(client)
	})

	/*
 	 * Get The Current Round Players
	*/
	client.on(C.PLAYERS_CRASH, () => {
        Crash.Players(client)
	})

	/*
 	 * Get The Crash Hisory
	*/
	client.on(C.HISTORY_CRASH, () => {
        Crash.getHistory(client)
	})

	/*
 	 * Play Crash
	*/
	client.on(C.PLAY_CRASH, (data) => {
       data = decode(data);
		Token.getID(data.token, (id) => {
			if(!id) return;
        	Crash.Play(client, io, _.toNumber(id), data)
        })
	})

	client.on('PLAY_JACKPOT', (data) => {
		data = decode(data);
		Token.getID(data.token, (id) => {
			if(!id) return;
			Crash.PlayJackpot(client, io, _.toNumber(id), data)
		})
	})

	client.on('AUTO_SIMPLE', (data) => {
		data = decode(data);
		Token.getID(data.token, (id) => {
			if(!id) {
				console.log('[temp] AUTO_SIMPLE: no user with token: ' + data.token);
				return
			};
			Crash.PlaySimple(client, io, _.toNumber(id), data)
		})
	})

	/*
 	 * CashOut Crash
	*/
	client.on(C.FINISH_CRASH, (data) => {
       	data = decode(data);
		Token.getID(data.token, (id) => {
			if(!id) return;
        	Crash.CashOut(client, io, _.toNumber(id), data, 'from index')
        })
	})
}

module.exports = Game;