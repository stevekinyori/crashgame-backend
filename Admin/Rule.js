const _ = require('lodash');
const bcrypt = require('bcryptjs');
const Model = require('../General/Model');
const H = require('../General/Helper');
const UserRule = require('../Users/Rule');
const config = require('../config');

const UPLOAD_URL = config.avatarUrl;

const Admin = {};

/*
 * Upload a Avatar
*/
Admin.uploadAvatar = function (name, avatar, callback) {
    let fullAvatar = UPLOAD_URL + avatar;
    Model.query('UPDATE users SET avatar = $1 WHERE name = $2', [fullAvatar, name], function (err, result) {
        if (err) {
            console.log('error admin role: 18', err)
            return callback(false);
        }
        return callback({status: true, file: name});
    });
}

/*
 * Mute a User
*/
Admin.changeMute = function (name, callback) {
    UserRule.getUserInfoByName(name, (result, error) => {
        let id = _.toNumber(result.id);
        let muted = result.muted;
        let set;

        if (muted) {
            set = false
        } else {
            set = true
        }
        Model.query('UPDATE users SET muted = $1 WHERE id = $2', [set, id], (err, results) => {
            if (err) {
                console.log('error muted: ', err);
                callback(false, true)
            } else callback(true, false)
        })
    })
}


/*
 * Add a Chat
*/
Admin.addChat = function (name, country, message, callback) {
    country = _.lowerCase(country);

    UserRule.getIdByName(name, (id, err) => {
        if (err) return;

        id = _.toNumber(id);

        Model.query('SELECT avatar, level FROM users WHERE id = $1', [id], function (err, results) {
            if (err) {
                console.log('error on admin: 14', err)
                return callback(false, true);
            }

            if (results === 'undefined') return;

            let time = H.getCurrentTime(new Date());
            let sorter = _.toNumber(new Date().getTime());
            let avatar = results.rows[0].avatar;
            let level = results.rows[0].level;
            let table = 'chat_' + country;

            Model.query('INSERT INTO ' + table + '(name, uid, avatar, message, time, sorter) VALUES($1, $2, $3, $4, $5, $6)',
                [name, id, avatar, message, time, sorter],
                function (err, result) {
                    if (err) {
                        return callback(false, true)
                    } else {
                        level = (level !== undefined) ? level : 1;
                        callback({
                            country: country,
                            message: message,
                            name: name,
                            avatar: avatar,
                            time: time,
                            date: H.getCurrentDate(new Date()),
                            sorter: sorter,
                            level: level
                        }, false);
                    }
                });
        });
    });
};

/*
 * Create User
*/
function CreateUser(data, callback) {
    let {username, email, password, avatar} = data;

    let PROFIT = {usdt: '0.00000000'};
    let BALANCE = {usdt: '0.10000010'};
    const WALLET = {"usdt": {"trc20": null, "bep20": null, "erc20": null}};

    bcrypt.hash(password, 10, (err, hash) => {
        var id = _.toNumber(H.randomIntger(7)),
            friends = "Support",
            balance = JSON.stringify(BALANCE),
            wallet = JSON.stringify(WALLET),
            profit_high = JSON.stringify(PROFIT),
            profit_low = JSON.stringify(PROFIT),
            max_profit = JSON.stringify(PROFIT),
            profit = JSON.stringify(PROFIT);

        Model.query('INSERT INTO users(name, email, password, avatar, id, friends, balance, wallet, profit_high, profit_low, profit, max_profit) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)',
            [username, email, hash, avatar, id, friends, balance, wallet, profit_high, profit_low, profit, max_profit], function (err, result) {
                if (err) {
                    console.log('error on adminRule: 123', err)
                    return callback(false, true);
                } else callback(id, false);
            });
    });
}


/*
 * Run A Custom Database Query
*/
Admin.runQuery = function (query, callback) {
    Model.query(query, function (error, result) {
        callback({error, result});
    })
}

module.exports = Admin