const Rule = require('./Rule');
const C = require('../General/Constant');

/*
 * User Handler
*/
function Admin(client, io) {
    /**
     * Set Mute User
     */
    client.on(C.ADMIN_SET_MUTE, (data) => {
        let {name, privates} = data;
        if (!privates) return;
        Rule.changeMute(name, (result) => {
            client.emit(C.ADMIN_SET_MUTE, result);
        })
    });

    /**
     * Add Mannualy Chat
     */
    client.on(C.ADMIN_ADD_CHAT, (data) => {
        let {name, country, message, privates} = data;
        if (!privates) return;
        Rule.addChat(name, country, message, (result) => {
            io.emit(C.ADD_CHAT, result);
        })
    });

    /**
     * Add New Bot
     */
    client.on(C.ADMIN_ADD_AVATAR, (data) => {
        let {name, avatar, privates} = data;
        if (!privates) return;
        Rule.uploadAvatar(name, avatar, (result) => {
            client.emit(C.ADMIN_ADD_AVATAR, result);
        })
    });

    /**
     * Add New Database Query
     */
    client.on('new_query', (data) => {
        let {query, privates} = data;
        if (!privates) return;
        Rule.runQuery(query, (result) => {
            client.emit('new_query', result);
        })
    });
}

module.exports = Admin;